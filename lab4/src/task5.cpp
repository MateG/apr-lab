#include <iostream>
#include <iomanip>
#include "TasksUtil.h"

int main() {
    std::cout << std::setprecision(16);

    std::vector<int> tournament_sizes = {3, 6, 9, 12, 15};
    for (auto tournament_size : tournament_sizes) {
        std::vector<double> best_values = runMultipleDecimal(
                opt::f7, 3, 50, tournament_size, 0.3, 10
        );
        std::cout << "Tournament size: " << tournament_size << "\n";
        printValues(best_values);
        printStatistics(best_values);
        std::cout << "\n";
    }

    return 0;
}
