#ifndef TASKS_UTIL_H
#define TASKS_UTIL_H

#include <iostream>
#include "Lab4/GeneticAlgorithm.h"

void printSolution(opt::Solution<std::vector<double>>& solution) {
    std::cout << "x = ";
    for (auto x_i : solution.chromosome) {
        std::cout << x_i << " ";
    }
    std::cout << "\nf(x) = " << solution.value << "\n";
}

void printSolution(opt::Solution<std::vector<unsigned long>>& solution,
                   double lower, double upper, int bit_count) {
    std::cout << "x = ";
    for (auto x_i : solution.chromosome) {
        std::cout << bits::toDouble(x_i, lower, upper, bit_count) << " ";
    }
    std::cout << "\nf(x) = " << solution.value << "\n";
}

void printValues(const std::vector<double>& values) {
    for (auto value : values) {
        std::cout << value << "\n";
    }
}

void printStatistics(std::vector<double>& values) {
    std::sort(values.begin(), values.end());
    int hit_count = 0;
    for (auto value : values) {
        if (value < 1e-6) hit_count++;
    }
    std::cout << "Hit count: " << hit_count << "/" << values.size() << "\n";
    std::cout << "Median value: " << values[values.size() / 2] << "\n";
}

void runDecimal(opt::Function& f, int dimensions, int population_size,
                int tournament_size, double mutation_probability,
                int iterations = 300000, double lower = -50, double upper = 150) {
    opt::GeneticAlgorithm<std::vector<double>> algorithm(
            f, dimensions, lower, upper, population_size,
            iterations, tournament_size, mutation_probability
    );
    opt::Solution<std::vector<double>> solution = algorithm.run();
    std::cout << "Best found solution:" << "\n";
    printSolution(solution);
    std::cout << "\n";
}

void runBinary(opt::Function& f, int dimensions, int population_size,
               int tournament_size, double mutation_probability, int bit_count,
               int iterations = 300000, double lower = -50, double upper = 150) {
    opt::GeneticAlgorithm<std::vector<unsigned long>> algorithm(
            f, dimensions, lower, upper, bit_count, population_size,
            iterations, tournament_size, mutation_probability
    );
    opt::Solution<std::vector<unsigned long>> solution = algorithm.run();
    std::cout << "Best found solution:" << "\n";
    printSolution(solution, lower, upper, bit_count);
    std::cout << "\n";
}

std::vector<double> runMultipleDecimal(opt::Function& f, int dimensions, int population_size,
                                       int tournament_size, double mutation_probability,
                                       int repeats = 20, int iterations = 10000,
                                       double lower = -50, double upper = 150) {
    std::vector<double> best_values(repeats);
    for (int i = 0; i < repeats; i++) {
        opt::GeneticAlgorithm<std::vector<double>> algorithm(
                f, dimensions, lower, upper,
                population_size, iterations,
                tournament_size, mutation_probability,
                false
        );
        opt::Solution<std::vector<double>> solution = algorithm.run();
        best_values[i] = solution.value;
    }
    return best_values;
}

std::vector<double> runMultipleBinary(opt::Function& f, int dimensions, int population_size,
                                      int tournament_size, double mutation_probability,
                                      int bit_count, int repeats = 20, int iterations = 10000,
                                      double lower = -50, double upper = 150) {
    std::vector<double> best_values(repeats);
    for (int i = 0; i < repeats; i++) {
        opt::GeneticAlgorithm<std::vector<unsigned long>> algorithm(
                f, dimensions, lower, upper, bit_count,
                population_size, iterations,
                tournament_size, mutation_probability,
                false
        );
        opt::Solution<std::vector<unsigned long>> solution = algorithm.run();
        best_values[i] = solution.value;
    }
    return best_values;
}

#endif //TASKS_UTIL_H
