#include <iostream>
#include <iomanip>
#include "TasksUtil.h"

int main() {
    std::cout << std::setprecision(16);

    std::cout << "Decimal:" << "\n";

    std::cout << "\nf1 [2D]:" << "\n";
    runDecimal(opt::f1, 2, 40, 3, 0.4);
    std::cout << "\nf3 [5D]:" << "\n";
    runDecimal(opt::f3, 5, 50, 3, 0.1);
    std::cout << "\nf6 [2D]:" << "\n";
    runDecimal(opt::f6, 2, 30, 3, 0.1);
    std::cout << "\nf7 [2D]:" << "\n";
    runDecimal(opt::f7, 2, 40, 3, 0.1);

    std::cout << "\nBinary:" << "\n";

    std::cout << "\nf1 [2D]:" << "\n";
    runBinary(opt::f1, 2, 40, 3, 0.7, 18);
    std::cout << "\nf3 [5D]:" << "\n";
    runBinary(opt::f3, 5, 40, 3, 0.1, 20);
    std::cout << "\nf6 [2D]:" << "\n";
    runBinary(opt::f6, 2, 30, 3, 0.1, 18);
    std::cout << "\nf7 [2D]:" << "\n";
    runBinary(opt::f7, 2, 30, 3, 0.2, 31);
}
