#ifndef UTIL_H
#define UTIL_H

#include <string>
#include <vector>
#include <sstream>

namespace util {

    /**
     * Utility function which converts the given vector to string.
     *
     * @param vector The given vector.
     * @return The corresponding string.
     */
    std::string vectorToString(const std::vector<double>& vector) {
        std::stringstream ss;
        ss << "[ ";
        for (auto value : vector) {
            ss << value << " ";
        }
        ss << "]";
        return ss.str();
    }
}

#endif //UTIL_H
