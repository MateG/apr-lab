#include "Lab4/ObjectiveFunctions.h"

#include <cmath>

using namespace opt;

Function opt::f1 = [](std::vector<double>& x) { // NOLINT(cert-err58-cpp)
    double x1 = x[0];
    double x2 = x[1];

    return 100.0 * std::pow(x2 - x1 * x1, 2) + std::pow(1.0 - x1, 2);
};

Function opt::f3 = [](std::vector<double>& x) { // NOLINT(cert-err58-cpp)
    double sum = 0.0;
    for (int i = 0; i < (int) x.size(); i++) {
        double difference = x[i] - (i + 1);
        sum += difference * difference;
    }

    return sum;
};

Function opt::f6 = [](std::vector<double>& x) { // NOLINT(cert-err58-cpp)
    double sum = 0.0;
    for (auto x_i : x) {
        sum += x_i * x_i;
    }

    double numerator = std::pow(std::sin(std::sqrt(sum)), 2) - 0.5;
    double denominator = std::pow(1 + 0.001 * sum, 2);

    return 0.5 + numerator / denominator;
};

Function opt::f7 = [](std::vector<double>& x) { // NOLINT(cert-err58-cpp)
    double sum = 0.0;
    for (auto x_i : x) {
        sum += x_i * x_i;
    }

    return std::pow(sum, 0.25) * (1 + std::pow(std::sin(50 * std::pow(sum, 0.1)), 2));
};
