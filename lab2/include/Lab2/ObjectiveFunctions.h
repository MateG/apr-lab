#ifndef OBJECTIVE_FUNCTIONS_H
#define OBJECTIVE_FUNCTIONS_H

#include <cmath>
#include "ObjectiveFunctionTypes.h"

namespace opt {

    /**
     * f(x) = 100 * (x2 - x1 ^ 2) ^ 2 + (1 - x1) ^ 2
     * x_min = (1, 1)
     * f_min = 0
     */
    extern Function rosenbrock_banana_function;

    /**
     * f(x) = (x1 - 4) ^ 2 + 4 * (x2 - 2) ^ 2
     * x_min = (4, 2)
     * f_min = 0
     */
    extern Function second_function;

    /**
     * f(x) = sum((xi - i) ^ 2)
     * x_min = (1, 2, 3, ..., n)
     * f_min = 0
     */
    extern Function third_function;

    /**
     * f(x) = |(x1 - x2) * (x1 + x2)| + sqrt(x1 ^ 2 + x2 ^ 2)
     * x_min = (0, 0)
     * f_min = 0
     */
    extern Function jakobovic_function;

    /**
     * f(x) = 0.5 + (sin^2(sqrt(xi ^ 2)) - 0.5) / (1 + 0.001 * sum(xi ^ 2)) ^ 2
     * x_min = (0, 0, 0, ..., 0)
     * f_min = 0
     */
    extern Function schaffer_function;

    /**
     * Wrapper class for std::function<double(Argument)>
     * which counts the number of function calls.
     *
     * @tparam Argument Wrapped function's argument.
     */
    template<typename Argument>
    class TrackedFunction {
    private:

        /** Wrapped function's type. */
        typedef std::function<double(Argument)> FunctionImpl;

        /** Wrapped function. */
        FunctionImpl& function_;

        /** Function call counter. */
        int call_count_;

    public:

        /**
         * Constructor which specifies the wrapped function
         * and initializes the call count to 0.
         *
         * @param function Specified function.
         */
        explicit TrackedFunction(FunctionImpl& function) : function_(function), call_count_(0) {};

        /**
         * Increments the call count and delegates
         * the call to the wrapped function.
         *
         * @param x Specified function argument.
         * @return Return value from the wrapped function call.
         */
        double operator()(Argument x) {
            call_count_++;
            return function_(x);
        };

        /** @return Call count. */
        int getCallCount() const {
            return call_count_;
        }

        /** Resets the call count to 0. */
        void Reset() {
            call_count_ = 0;
        }
    };
}

#endif //OBJECTIVE_FUNCTIONS_H
