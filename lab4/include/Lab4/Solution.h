#ifndef SOLUTION_H
#define SOLUTION_H

#include <utility>
#include <vector>

namespace opt {

    template<class C>
    struct Solution {
        C chromosome;
        double value;

        Solution() = default;

        explicit Solution(C chromosome) : chromosome(chromosome) {}
    };

//    template<>
//    struct Solution<std::vector<double>> {
//        std::vector<double> chromosome;
//        double value;
//
//        Solution() = default;
//
//        explicit Solution(std::vector<double>& chromosome)
//                : chromosome(chromosome) {}
//    };

//    struct BitVectorSolution : Solution {
//        std::vector<std::vector<bool>> chromosome;
//    };
}

#endif //SOLUTION_H
