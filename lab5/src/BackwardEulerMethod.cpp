#include "Lab5/BackwardEulerMethod.h"

std::vector<Matrix> backwardEuler(const Matrix& a, const Matrix& b, const RFunction& r,
                                  const Matrix& x0, const double T, const double t_max) {
    Matrix u = identityMatrix((int) a.getRows());
    Matrix p = !(u - a * T);
    Matrix q = p * T * b;
    IntegrationBiFunction calculate_next = [p, q, r, T](const Matrix& x, double t) {
        return p * x + q * r(t + T);
    };
    return integrationMethod(calculate_next, x0, T, t_max);
}

std::vector<Matrix> backwardEuler(const Matrix& a, const Matrix& x0, const double T, const double t_max) {
    Matrix u = identityMatrix((int) a.getRows());
    IntegrationBiFunction calculate_next = [u, a, T](const Matrix& x, double t) {
        return !(u - a * T) * x;
    };
    return integrationMethod(calculate_next, x0, T, t_max);
}
