#ifndef INTEGRATION_UTIL_H
#define INTEGRATION_UTIL_H

#include <iostream>
#include "Lab1/Matrix.h"

void printStep(const double t, const Matrix& x) {
    std::cout << t << " ";
    for (int i = 0; i < (int) x.getSize(); i++) {
        std::cout << x.Get(i) << " ";
    }
    std::cout << "\n";
}

void printSteps(const std::vector<Matrix>& x_all, const double T, const int log_period = 1) {
    double t = 0.0;
    int log_counter = 0;
    for (const Matrix& x : x_all) {
        if (log_counter == 0) {
            printStep(t, x);
            log_counter = log_period;
        }
        log_counter--;
        t += T;
    }
}

#endif //INTEGRATION_UTIL_H
