#ifndef MUTATION_H
#define MUTATION_H

#include <vector>
#include <random>

namespace opt {

    template<class C>
    class Mutation {
    public:
        void Mutate(C& chromosome) = 0;
    };

    template<>
    class Mutation<std::vector<double>> {
    private:
        std::random_device rd_;
        std::mt19937 mt_engine_ = std::mt19937(rd_());
        std::normal_distribution<> gaussian_ = std::normal_distribution<>(0.0, 5.0);
        std::uniform_real_distribution<> uniform_ = std::uniform_real_distribution<>(0.0, 1.0);

    public:
        void Mutate(std::vector<double>& chromosome) {
            const double probability = 0.25;

            for (auto& x_i : chromosome) {
                if (uniform_(mt_engine_) < probability) {
                    x_i += gaussian_(mt_engine_);
                }
            }
        }
    };

    template<>
    class Mutation<std::vector<unsigned long>> {
    private:

        int bit_count_;

        std::random_device rd_;
        std::mt19937 mt_engine_ = std::mt19937(rd_());
        std::uniform_real_distribution<> uniform_ = std::uniform_real_distribution<>(0.0, 1.0);
    public:

        explicit Mutation(int bit_count) : bit_count_(bit_count) {}

        Mutation(Mutation<std::vector<unsigned long>>&& other) noexcept
                : bit_count_(other.bit_count_) {}

        void Mutate(std::vector<unsigned long>& chromosome) {
            const double probability = 0.3;

            for (auto& x_i : chromosome) {
                for (int i = 0; i < bit_count_; i++) {
                    if (uniform_(mt_engine_) < probability) {
                        bits::invertBit(x_i, i);
                    }
                }
            }
        }
    };
}

#endif //MUTATION_H
