#ifndef OBJECTIVE_FUNCTIONS_H
#define OBJECTIVE_FUNCTIONS_H

#include <vector>
#include <functional>
#include "ObjectiveFunctionTypes.h"

namespace opt {

    /**
     * f(x) = 100 * (x2 - x1 ^ 2) ^ 2 + (1 - x1) ^ 2
     * x_min = (1, 1)
     * f_min = 0
     */
    extern Function f1;
    extern Derivation f1_gradient;
    extern Derivation f1_hessian;

    /**
     * f(x) = (x1 - 4) ^ 2 + 4 * (x2 - 2) ^ 2
     * x_min = (4, 2)
     * f_min = 0
     */
    extern Function f2;
    extern Derivation f2_gradient;
    extern Derivation f2_hessian;

    /**
     * f(x) = (x1 - 2) ^ 2 + (x2 + 3) ^ 2
     * x_min = (2, -3)
     * f_min = 0
     */
    extern Function f3;
    extern Derivation f3_gradient;

    /**
     * f(x) = (x1 - 3) ^ 2 + x2 ^ 2
     * x_min = (3, 0)
     * f_min = 0
     */
    extern Function f4;

    /**
     * Wrapper class for std::function<double(Argument)>
     * which counts the number of function calls.
     *
     * @tparam ReturnValue Wrapped function's return value.
     * @tparam Argument Wrapped function's argument.
     */
    template<typename ReturnValue, typename Argument>
    class TrackedFunction {
    private:

        /** Wrapped function's type. */
        typedef std::function<ReturnValue(Argument)> FunctionImpl;

        /** Wrapped function. */
        FunctionImpl& function_;

        /** Function call counter. */
        int call_count_;

    public:

        /**
         * Constructor which specifies the wrapped function
         * and initializes the call count to 0.
         *
         * @param function Specified function.
         */
        explicit TrackedFunction(FunctionImpl& function) : function_(function), call_count_(0) {};

        /**
         * Increments the call count and delegates
         * the call to the wrapped function.
         *
         * @param x Specified function argument.
         * @return Return value from the wrapped function call.
         */
        ReturnValue operator()(Argument x) {
            call_count_++;
            return function_(x);
        };

        /** @return Call count. */
        int getCallCount() const {
            return call_count_;
        }

        /** Resets the call count to 0. */
        void Reset() {
            call_count_ = 0;
        }
    };
}

#endif //OBJECTIVE_FUNCTIONS_H
