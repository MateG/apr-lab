#ifndef MY_TESTER_CORE_H
#define MY_TESTER_CORE_H

#include <string>
#include <functional>
#include <vector>
#include <iostream>

namespace MyTester {

    static std::vector<std::pair<std::string, std::function<void()>>> tests;

    static int tests_passed;
    static int tests_failed;
    static int asserts_passed;
    static int asserts_failed;

    void runTests() {
        tests_passed = 0;
        tests_failed = 0;

        for (auto& test : tests) {
            asserts_passed = 0;
            asserts_failed = 0;
            test.second();

            if (asserts_failed > 0) {
                tests_failed++;
                std::cout << " - [" << test.first << "] failed ("
                          << asserts_passed << "/" << asserts_passed + asserts_failed << ")\n";
            } else {
                tests_passed++;
                std::cout << "OK [" << test.first << "] passed ("
                          << asserts_passed << "/" << asserts_passed << ")\n";
            }
        }

        std::cout << (tests_failed ? "\nTESTS FAILED" : "")
                  << "\nRan: " << tests_passed + tests_failed
                  << ", passed: " << tests_passed
                  << ", failed: " << tests_failed << std::endl;
    }

    void addTest(const char* name, std::function<void()> callback) {
        tests.emplace_back(name, callback);
    }

    void assertTrue(bool expression) {
        expression ? asserts_passed++ : asserts_failed++;
    }
}

#endif //MY_TESTER_CORE_H
