#ifndef GENETIC_ALGORITHM_H
#define GENETIC_ALGORITHM_H

#include <vector>
#include <algorithm>
#include "ChromosomeGenerator.h"
#include "ObjectiveFunctions.h"
#include "Crossover.h"
#include "Mutation.h"
#include "Decoder.h"

namespace opt {

    template<class C>
    class GeneticAlgorithm {

    private:

        TrackedFunction f_;
        int population_size_;
        int max_evaluations_;
        int tournament_size_;
        double mutation_probability_;
        ChromosomeGenerator<C> chromosome_generator_;
        Crossover<C> crossover_;
        Mutation<C> mutation_;
        Decoder<C> decoder_;

        bool logging_;

        std::random_device rd_;
        std::mt19937 mt_engine_;
        std::uniform_real_distribution<> uniform_;
        std::uniform_int_distribution<> population_index_uniform_;

    public:

        GeneticAlgorithm(
                Function f,
                int dimensions,
                double lower,
                double upper,
                int population_size,
                int max_evaluations,
                int tournament_size,
                double mutation_probability,
                bool logging = true
        ) : f_(TrackedFunction(f)),
            population_size_(population_size),
            max_evaluations_(max_evaluations),
            tournament_size_(tournament_size),
            mutation_probability_(mutation_probability),
            chromosome_generator_(ChromosomeGenerator<C>(dimensions, lower, upper)),
            mt_engine_(std::mt19937(rd_())),
            uniform_(std::uniform_real_distribution<>(0.0, 1.0)),
            population_index_uniform_(std::uniform_int_distribution<>(0, population_size - 1)),
            logging_(logging) {}

        GeneticAlgorithm(
                Function f,
                int dimensions,
                double lower,
                double upper,
                int bit_count,
                int population_size,
                int max_evaluations,
                int tournament_size,
                double mutation_probability,
                bool logging = true
        ) : f_(TrackedFunction(f)),
            population_size_(population_size),
            max_evaluations_(max_evaluations),
            tournament_size_(tournament_size),
            mutation_probability_(mutation_probability),
            chromosome_generator_(ChromosomeGenerator<C>(dimensions, lower, upper, bit_count)),
            crossover_(Crossover<C>(bit_count)),
            mutation_(Mutation<C>(bit_count)),
            decoder_(Decoder<C>(lower, upper, bit_count)),
            mt_engine_(std::mt19937(rd_())),
            uniform_(std::uniform_real_distribution<>(0.0, 1.0)),
            population_index_uniform_(std::uniform_int_distribution<>(0, population_size - 1)),
            logging_(logging) {}

        Solution<C> run() {
            // Create population
            std::vector<Solution<C>> population(population_size_);
            for (int i = 0; i < population_size_; i++) {
                population[i].chromosome = chromosome_generator_.GenerateChromosome();
            }

            // Evaluate population
            for (int i = 0; i < population_size_; i++) {
                population[i].value = f_(decoder_.Decode(population[i].chromosome));
            }

            // Find current best
            int best_index = 0;
            double best_value = population[best_index].value;
            for (int i = 1; i < population_size_; i++) {
                double value = population[i].value;
                if (value < best_value) {
                    best_value = value;
                    best_index = i;
                }
            }
            if (best_value < 1e-6) return population[best_index];

            std::vector<int> tournament_indexes(tournament_size_);
            while (f_.getCallCount() < max_evaluations_) {
                // Select k solutions for tournament
                fillRandomIndexes(tournament_indexes);

                // Sort the tournament by value (ascending)
                std::sort(
                        tournament_indexes.begin(),
                        tournament_indexes.end(),
                        [population](const int& a, const int& b) {
                            return population[a].value < population[b].value;
                        }
                );

                // Create child using the 2 best parents from the tournament
                int first_parent_index = tournament_indexes[0];
                int second_parent_index = tournament_indexes[1];
                int worst_index = tournament_indexes[tournament_indexes.size() - 1];
                population[worst_index].chromosome = crossover_.Cross(
                        population[first_parent_index].chromosome,
                        population[second_parent_index].chromosome
                );

                // Mutate child (with mutation probability)
                if (uniform_(mt_engine_) < mutation_probability_) {
                    mutation_.Mutate(population[worst_index].chromosome);
                }

                // Evaluate child
                double value = f_(decoder_.Decode(population[worst_index].chromosome));
                population[worst_index].value = value;

                // Update current best
                if (value < best_value) {
                    best_value = value;
                    best_index = worst_index;
                    if (logging_) {
                        std::cout << "After " << f_.getCallCount()
                                  << " evaluations, found new best: " << best_value << "\n";
                    }

                    if (value < 1e-6) {
                        return population[worst_index];
                    }
                }
            }

            std::sort(population.begin(), population.end(),
                      [](const Solution<C>& a, const Solution<C>& b) {
                          return a.value < b.value;
                      }
            );
            return population[0];
        }

    private:

        void fillRandomIndexes(std::vector<int>& indexes) {
            for (int i = 0; i < tournament_size_; i++) {
                int random_index;
                while (true) {
                    random_index = population_index_uniform_(mt_engine_);
                    bool index_taken = false;
                    for (int j = i - 1; j >= 0; j--) {
                        if (indexes[j] == random_index) {
                            index_taken = true;
                            break;
                        }
                    }
                    if (!index_taken) break;
                }
                indexes[i] = random_index;
            }
        }
    };
}

#endif //GENETIC_ALGORITHM_H
