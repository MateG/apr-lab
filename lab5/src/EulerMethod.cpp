#include "Lab5/EulerMethod.h"

std::vector<Matrix> euler(const Matrix& a, const IntegrationBiFunction& x_derivation,
                          const Matrix& x0, const double T, const double t_max) {
    IntegrationBiFunction calculate_next = [x_derivation, T](const Matrix& x, double t) {
        return x + x_derivation(x, t) * T;
    };
    return integrationMethod(calculate_next, x0, T, t_max);
}

std::vector<Matrix> euler(const Matrix& a, const Matrix& b, const RFunction& r,
                          const Matrix& x0, const double T, const double t_max) {
    IntegrationBiFunction x_derivation = [a, b, r](const Matrix& x, double t) {
        return a * x + b * r(t);
    };
    return euler(a, x_derivation, x0, T, t_max);
}

std::vector<Matrix> euler(const Matrix& a, const Matrix& x0, const double T, const double t_max) {
    IntegrationBiFunction x_derivation = [a](const Matrix& x, double t) {
        return a * x;
    };
    return euler(a, x_derivation, x0, T, t_max);
}
