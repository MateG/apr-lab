#include <iostream>
#include <iomanip>
#include "TasksUtil.h"

int main() {
    std::cout << std::setprecision(16);

    std::vector<double> best_values = runMultipleDecimal(opt::f6, 3, 30, 3, 0.1);
    std::cout << "f6 [3D] decimal:\n";
    printValues(best_values);
    printStatistics(best_values);

    best_values = runMultipleBinary(opt::f6, 3, 30, 3, 0.1, 21);
    std::cout << "\nf6 [3D] binary:\n";
    printValues(best_values);
    printStatistics(best_values);

    best_values = runMultipleDecimal(opt::f6, 6, 30, 3, 0.1);
    std::cout << "\nf6 [6D] decimal:\n";
    printValues(best_values);
    printStatistics(best_values);

    best_values = runMultipleBinary(opt::f6, 6, 30, 3, 0.1, 21);
    std::cout << "\nf6 [6D] binary:\n";
    printValues(best_values);
    printStatistics(best_values);

    best_values = runMultipleDecimal(opt::f7, 3, 50, 3, 0.3);
    std::cout << "\nf7 [3D] decimal:\n";
    printValues(best_values);
    printStatistics(best_values);

    best_values = runMultipleBinary(opt::f7, 3, 50, 3, 0.3, 21);
    std::cout << "\nf7 [3D] binary:\n";
    printValues(best_values);
    printStatistics(best_values);

    best_values = runMultipleDecimal(opt::f7, 6, 150, 3, 0.5);
    std::cout << "\nf7 [6D] decimal:\n";
    printValues(best_values);
    printStatistics(best_values);

    best_values = runMultipleBinary(opt::f7, 6, 150, 3, 0.5, 21);
    std::cout << "\nf7 [6D] binary:\n";
    printValues(best_values);
    printStatistics(best_values);

    return 0;
}
