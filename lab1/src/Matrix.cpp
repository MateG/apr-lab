#include "Lab1/Matrix.h"

#include <fstream>
#include <sstream>
#include <algorithm>
#include <vector>

/** Various helper non-member functions. */
namespace matrix_private {

    /**
     * Compares the specified values using epsilon comparison.
     *
     * @param a First value.
     * @param b Second value.
     * @return True if values are considered equal, false otherwise.
     */
    inline bool equalValues(double a, double b) {
        return std::abs(a - b) < 1e-6;
    }

    /**
     * Fills the specified vector with default permutation values (and a zero
     * value at the end that can be used as a permutation counter).
     *
     * @param P Specified vector.
     * @param size Specified size. Actual vector size will be larger by 1.
     */
    void fillPermutationVector(std::vector<int>& P, int size) {
        P.reserve(size + 1);
        for (int i = 0; i < size; i++) P.push_back(i);
        P.push_back(0);
    }

    /**
     * Resets the specified file stream to the beginning.
     *
     * @param file_stream Specified file stream.
     */
    void resetFileStream(std::ifstream& file_stream) {
        file_stream.clear();
        file_stream.seekg(0, std::ios::beg);
    }

    /**
     * Counts the rows in the specified file stream.
     *
     * @param file_stream Specified file stream.
     * @return Row count.
     */
    unsigned rowsFromFile(std::ifstream& file_stream) {
        auto rows = std::count(
                std::istreambuf_iterator<char>(file_stream),
                std::istreambuf_iterator<char>(), '\n') + 1;

        resetFileStream(file_stream);
        return static_cast<unsigned>(rows);
    }

    /**
     * Counts the columns in the first row of the specified file stream.
     *
     * @param file_stream Specified file stream.
     * @return Column count.
     */
    unsigned colsFromFile(std::ifstream& file_stream) {
        std::string row;
        getline(file_stream, row);
        std::istringstream first_row_stream(row);

        unsigned cols = 0;
        double value;
        while (first_row_stream >> value) cols++;

        resetFileStream(file_stream);
        return cols;
    }

    /**
     * Formats the given dimensions as a string.
     *
     * @param rows Row count.
     * @param cols Column count.
     * @return Formatted dimensions string.
     */
    std::string dimensionsString(unsigned rows, unsigned cols) {
        return std::to_string(rows) + "x" + std::to_string(cols);
    }

    /**
     * Checks if the dimensions of the specified matrices match.
     *
     * @param first First matrix.
     * @param second Second matrix.
     */
    void checkDimensionsMatch(const Matrix& first, const Matrix& second) {
        unsigned r1 = first.getRows();
        unsigned c1 = first.getCols();
        unsigned r2 = second.getRows();
        unsigned c2 = second.getCols();

        if (r1 != r2 || c1 != c2) {
            throw std::invalid_argument(
                    "Matrix dimensions don't match: "
                    + dimensionsString(r1, c1) + " <> "
                    + dimensionsString(r2, c2));
        }
    }

    /**
     * Checks if the specified matrix is a square matrix.
     *
     * @param matrix Specified matrix.
     */
    void checkSquareDimension(const Matrix& matrix) {
        unsigned rows = matrix.getRows();
        unsigned cols = matrix.getCols();

        if (rows != cols) {
            throw std::invalid_argument(
                    "Matrix is not square: "
                    + dimensionsString(rows, cols));
        }
    }

    /**
     * Checks if the specified matrices are compatible for multiplication.
     *
     * @param first First matrix.
     * @param second Second matrix.
     */
    void checkMultiplyDimensions(const Matrix& first, const Matrix& second) {
        unsigned c1 = first.getCols();
        unsigned r2 = second.getRows();

        if (c1 != r2) {
            throw std::invalid_argument(
                    "Incompatible matrices for multiplication: "
                    + dimensionsString(first.getRows(), c1) + " <> "
                    + dimensionsString(r2, second.getCols()));
        }
    }

    /**
     * Checks if the specified matrix has the expected dimensions.
     *
     * @param matrix Specified matrix.
     * @param rows Expected row count.
     * @param cols Expected column count.
     */
    void checkDimensions(const Matrix& matrix, unsigned rows, unsigned cols) {
        unsigned matrix_rows = matrix.getRows();
        unsigned matrix_cols = matrix.getCols();

        if (matrix_rows != rows || matrix_cols != cols) {
            throw std::invalid_argument(
                    "Expected matrix dimensions: "
                    + dimensionsString(rows, cols)
                    + ", but was: "
                    + dimensionsString(matrix_rows, matrix_cols));
        }
    }

    /**
     * Checks if the specified divisor is different than zero.
     *
     * @param divisor Specified divisor.
     * @return Unaltered divisor.
     */
    double checkDivisor(double divisor) {
        if (equalValues(divisor, 0.0)) {
            throw std::invalid_argument("Division by zero occurred!");
        }
        return divisor;
    }
}

using namespace matrix_private;

Matrix::Matrix(unsigned rows, unsigned cols, bool zero_init) {
    allocateData(rows, cols);
    if (zero_init) {
        for (int i = 0; i < size_; i++) data_[i] = 0;
    }
}

Matrix::Matrix(const std::string& file_path) {
    // Open file stream
    std::ifstream file_stream(file_path);
    if (!file_stream.is_open()) {
        throw std::invalid_argument("Invalid file path: " + file_path);
    }

    // Set row/col count
    unsigned rows = rowsFromFile(file_stream);
    unsigned cols = colsFromFile(file_stream);

    // Populate with data
    allocateData(rows, cols);
    std::string row;
    for (unsigned row_index = 0, data_index = 0; row_index < rows; row_index++) {
        getline(file_stream, row);
        std::istringstream row_stream(row);
        for (unsigned col_index = 0; col_index < cols; col_index++, data_index++) {
            row_stream >> data_[data_index];
        }
    }

    file_stream.close();
}

Matrix::Matrix(const Matrix& original) {
    allocateData(original.rows_, original.cols_);
    std::copy(original.data_, original.data_ + size_, data_);
}

Matrix::Matrix(unsigned rows, unsigned cols, std::initializer_list<double> values) {
    allocateData(rows, cols);
    std::copy(values.begin(), values.end(), data_);
}

Matrix::~Matrix() {
    deallocateData();
}

unsigned Matrix::getRows() const {
    return rows_;
}

unsigned Matrix::getCols() const {
    return cols_;
}

unsigned Matrix::getSize() const {
    return size_;
}

Matrix& Matrix::operator=(const Matrix& other) {
    // Self-assignment
    if (this == &other) return *this;

    // Reallocate array (if needed)
    if (rows_ != other.rows_ || cols_ != other.cols_) {
        deallocateData();
        allocateData(other.rows_, other.cols_);
    }

    // Copy data
    std::copy(other.data_, other.data_ + size_, data_);
    return *this;
}

double* Matrix::operator[](unsigned row) {
    return &data_[row * cols_];
}

Matrix Matrix::operator+(const Matrix& other) const {
    checkDimensionsMatch(*this, other);

    Matrix result = *this;
    result += other;
    return result;
}

void Matrix::operator+=(const Matrix& other) {
    checkDimensionsMatch(*this, other);

    for (unsigned i = 0; i < size_; i++) {
        data_[i] += other.data_[i];
    }
}

Matrix Matrix::operator-(const Matrix& other) const {
    checkDimensionsMatch(*this, other);

    Matrix result = *this;
    result -= other;
    return result;
}

void Matrix::operator-=(const Matrix& other) {
    checkDimensionsMatch(*this, other);

    for (unsigned i = 0; i < size_; i++) {
        data_[i] -= other.data_[i];
    }
}

Matrix Matrix::operator*(double scalar) const {
    Matrix result = *this;
    result *= scalar;
    return result;
}

void Matrix::operator*=(double scalar) {
    for (unsigned i = 0; i < size_; i++) {
        data_[i] *= scalar;
    }
}

Matrix Matrix::operator*(const Matrix& other) const {
    checkMultiplyDimensions(*this, other);

    Matrix result(this->rows_, other.cols_);
    for (int i = 0; i < result.rows_; i++) {
        for (int k = 0; k < result.cols_; k++) {
            result[i][k] = 0.0;
            for (int j = 0; j < cols_; j++) {
                double a = this->data_[i * this->cols_ + j];
                double b = other.data_[j * other.cols_ + k];
                result[i][k] += a * b;
            }
        }
    }
    return result;
}

#pragma clang diagnostic push
#pragma ide diagnostic ignored "ArgumentSelectionDefects"

Matrix Matrix::operator~() const {
    Matrix result(cols_, rows_); // Swapped dimensions
    for (unsigned row = 0, data_index = 0; row < rows_; row++) {
        for (unsigned col = 0; col < cols_; col++, data_index++) {
            result[col][row] = data_[data_index];
        }
    }
    return result;
}

#pragma clang diagnostic pop

Matrix Matrix::operator!() const {
    checkSquareDimension(*this);

    // Perform LUP
    Matrix lu = *this;
    std::vector<int> P;
    lu.lup(P);

    // Allocate inverse without data initialization
    Matrix inverse(rows_, rows_, false);

    // Zero vector
    Matrix b_i(rows_, 1);
    for (int i = 0; i < rows_; i++) {
        // Update b_i as permuted e_i
        b_i[P[P[i]]][0] = 1;

        // Solve for i-th x vector
        Matrix y_i = lu.ForwardSubstitution(b_i);
        Matrix x_i = lu.BackSubstitution(y_i);

        // Put x_i vector in i-th column of the inverse
        inverse.setVector(x_i, i);

        // Reset b_i to zero vector
        b_i[P[P[i]]][0] = 0;
    }

    return inverse;
}

bool Matrix::operator==(const Matrix& other) const {
    if (rows_ != other.rows_ || cols_ != other.cols_) return false;

    for (int i = 0; i < size_; i++) {
        if (!equalValues(data_[i], other.data_[i])) return false;
    }

    return true;
}

bool Matrix::operator!=(const Matrix& other) const {
    return !(*this == other);
}

Matrix Matrix::ForwardSubstitution(const Matrix& vector) const {
    checkSquareDimension(*this);
    checkDimensions(vector, rows_, 1);

    Matrix y = vector;
    for (int i = 0; i < rows_ - 1; i++) {
        for (int j = i + 1; j < rows_; j++) {
            y[j][0] -= data_[j * cols_ + i] * y[i][0];
        }
    }
    return y;
}

Matrix Matrix::BackSubstitution(const Matrix& vector) const {
    checkSquareDimension(*this);
    checkDimensions(vector, rows_, 1);

    Matrix x = vector;
    for (int i = rows_ - 1; i >= 0; i--) {
        x[i][0] /= checkDivisor(data_[i * cols_ + i]);
        for (int j = 0; j < i; j++) {
            x[j][0] -= data_[j * cols_ + i] * x[i][0];
        }
    }
    return x;
}

Matrix Matrix::LuDecomposition() const {
    Matrix result = *this;
    result.LuDecompose();
    return result;
}

void Matrix::LuDecompose() {
    checkSquareDimension(*this);

    int n = rows_;
    for (int i = 0; i < n - 1; i++) {
        for (int j = i + 1; j < n; j++) {
            (*this)[j][i] /= checkDivisor((*this)[i][i]);
            for (int k = i + 1; k < n; k++) {
                (*this)[j][k] -= (*this)[j][i] * (*this)[i][k];
            }
        }
    }
}

Matrix Matrix::LupDecomposition(std::vector<int>& P) const {
    Matrix result = *this;
    result.LupDecompose(P);
    return result;
}

void Matrix::LupDecompose(std::vector<int>& P) {
    checkSquareDimension(*this);
    lup(P);
}

double Matrix::Determinant() const {
    Matrix lu = *this;
    std::vector<int> P;
    lu.lup(P);

    double determinant = (P[rows_] % 2 == 0) ? 1.0 : -1.0;
    for (int i = 0; i < rows_; i++) {
        determinant *= lu[i][i];
    }

    return determinant;
}

Matrix Matrix::Permutation(const std::vector<int>& P) {
    Matrix result = *this;
    result.Permute(P);
    return result;
}

void Matrix::Permute(const std::vector<int>& P) {
    Matrix copy = *this;
    for (int i = 0; i < rows_; i++) {
        if (i == P[i]) continue;

        double* source_start = copy.data_ + P[i] * cols_;
        double* source_end = source_start + cols_;
        double* destination_start = data_ + i * cols_;
        std::copy(source_start, source_end, destination_start);
    }
}

std::ostream& operator<<(std::ostream& stream, const Matrix& matrix) {
    for (unsigned row = 0, data_index = 0; row < matrix.rows_; row++) {
        for (unsigned col = 0; col < matrix.cols_; col++, data_index++) {
            stream << matrix.data_[data_index] << ' ';
        }
        stream << '\n';
    }
    return stream;
}

void Matrix::allocateData(unsigned rows, unsigned cols) {
    rows_ = rows;
    cols_ = cols;
    size_ = rows * cols;
    data_ = new double[rows * cols];
}

void Matrix::deallocateData() {
    delete[] data_;
}

void Matrix::lup(std::vector<int>& P) {
    fillPermutationVector(P, rows_);

    for (int i = 0; i < rows_ - 1; i++) {
        int pivot = i;
        for (int j = i + 1; j < rows_; j++) {
            if (std::abs((*this)[P[j]][i]) > std::abs((*this)[P[pivot]][i])) {
                pivot = j;
            }
        }

        // Store permutation and increment counter
        if (i != pivot) {
            std::swap(P[i], P[pivot]);
            P[rows_]++;
        }

        for (int j = i + 1; j < rows_; j++) {
            (*this)[P[j]][i] /= checkDivisor((*this)[P[i]][i]);
            for (int k = i + 1; k < rows_; k++) {
                (*this)[P[j]][k] -= (*this)[P[j]][i] * (*this)[P[i]][k];
            }
        }
    }

    Permute(P);
}

void Matrix::setVector(const Matrix& vector, int col) {
    for (int i = 0; i < rows_; i++) {
        (*this)[i][col] = vector.data_[i];
    }
}
