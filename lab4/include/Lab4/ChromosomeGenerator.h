#ifndef SOLUTION_GENERATOR_H
#define SOLUTION_GENERATOR_H

#include <random>
#include "Solution.h"

namespace opt {

    template<class C>
    class ChromosomeGenerator {

    public:
        C GenerateChromosome() = 0;
    };

    template<>
    class ChromosomeGenerator<std::vector<double>> {

    private:
        int dimensions_;

        std::random_device rd_;
        std::mt19937 mt_engine_;
        std::uniform_real_distribution<> uniform_;

    public:
        ChromosomeGenerator(int dimensions, double lower, double upper)
                : dimensions_(dimensions),
                  mt_engine_(std::mt19937(rd_())),
                  uniform_(std::uniform_real_distribution<>(lower, upper)) {}

        ChromosomeGenerator(ChromosomeGenerator<std::vector<double>>&& other) noexcept
                : dimensions_(other.dimensions_),
                  mt_engine_(other.mt_engine_),
                  uniform_(other.uniform_) {}

        std::vector<double> GenerateChromosome() {
            std::vector<double> chromosome(dimensions_);
            for (double& x_i : chromosome) {
                x_i = uniform_(mt_engine_);
            }
            return chromosome;
        }
    };

    template<>
    class ChromosomeGenerator<std::vector<unsigned long>> {

    private:

        int dimensions_;
        int bit_count_;

        std::random_device rd_;
        std::mt19937 mt_engine_;

    public:

        ChromosomeGenerator(int dimensions, double lower, double upper, int bit_count)
                : dimensions_(dimensions),
                  bit_count_(bit_count),
                  mt_engine_(rd_()) {}

        ChromosomeGenerator(ChromosomeGenerator<std::vector<unsigned long>>&& other) noexcept
                : dimensions_(other.dimensions_),
                  bit_count_(other.bit_count_),
                  mt_engine_(other.mt_engine_) {}

    public:

        std::vector<unsigned long> GenerateChromosome() {
            std::vector<unsigned long> chromosome(dimensions_);
            std::uniform_int_distribution<> uniform(0, (1u << unsigned(bit_count_)) - 1);
            for (int i = 0; i < dimensions_; i++) {
                chromosome[i] = uniform(mt_engine_);
            }
            return chromosome;
        }
    };
}

#endif //SOLUTION_GENERATOR_H
