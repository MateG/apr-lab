#include "Lab1/Matrix.h"
#include "MyTester/Macro.h"

int main() {
    TEST("Dimensions constructor", [] {
        Matrix a(2, 3);

        ASSERT(a.getRows() == 2);
        ASSERT(a.getCols() == 3);
        ASSERT(a.getSize() == 6);
    });

    TEST("Load from file", [] {
        Matrix a("../data/2a.txt");

        ASSERT(a.getRows() == 3);
        ASSERT(a.getCols() == 3);
        ASSERT(a.getSize() == 9);

        ASSERT(a[0][0] == 3);
        ASSERT(a[0][1] == 9);
        ASSERT(a[0][2] == 6);
        ASSERT(a[1][0] == 4);
        ASSERT(a[1][1] == 12);
        ASSERT(a[1][2] == 12);
        ASSERT(a[2][0] == 1);
        ASSERT(a[2][1] == -1);
        ASSERT(a[2][2] == 1);
    });

    TEST("Copy constructor", [] {
        Matrix a("../data/2a.txt");
        Matrix b = a;
        ASSERT(a == b);

        b[0][0] = 999;
        ASSERT(a != b);
        ASSERT(b[0][0] == 999);
        ASSERT(a[0][0] != 999);
    });

    TEST("Initializer list constructor", [] {
        Matrix a(3, 1, {4, 3, 2});
        ASSERT(a.getRows() == 3);
        ASSERT(a.getCols() == 1);
        ASSERT(a[0][0] == 4);
        ASSERT(a[1][0] == 3);
        ASSERT(a[2][0] == 2);

        Matrix b(2, 3, {1, 2, 3, 4, 5, 6});
        ASSERT(b.getRows() == 2);
        ASSERT(b.getCols() == 3);
        ASSERT(b[0][0] == 1);
        ASSERT(b[0][1] == 2);
        ASSERT(b[0][2] == 3);
        ASSERT(b[1][0] == 4);
        ASSERT(b[1][1] == 5);
        ASSERT(b[1][2] == 6);
    });

    TEST("Assignment operator", [] {
        Matrix a("../data/2a.txt");
        Matrix b(7, 9);
        ASSERT(a != b);

        b = a;
        ASSERT(a == b);

        b[0][0] = 999;
        ASSERT(a != b);
        ASSERT(b[0][0] == 999);
        ASSERT(a[0][0] != 999);
    });

    TEST("Square bracket operator", [] {
        Matrix a("../data/2a.txt");
        double* first_row = a[0];

        ASSERT(first_row[0] == 3);
        ASSERT(first_row[1] == 9);
        ASSERT(first_row[2] == 6);
        first_row[2] = 999;
        ASSERT(a[0][2] == 999);
        a[0][2] = 6;
        ASSERT(first_row[2] == 6);
    });

    TEST("Add operator", [] {
        Matrix a("../data/2a.txt");
        Matrix b = a;

        b[0][1] = 99;
        b[2][0] = 999;
        Matrix c = a + b;

        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                ASSERT(c[row][col] == a[row][col] + b[row][col]);
            }
        }

        Matrix d = a;
        d += b;

        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                ASSERT(d[row][col] == a[row][col] + b[row][col]);
            }
        }
    });

    TEST("Subtract operator", [] {
        Matrix a("../data/2a.txt");
        Matrix b = a;

        b[0][1] = 99;
        b[2][0] = 999;
        Matrix c = a - b;

        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                ASSERT(c[row][col] == a[row][col] - b[row][col]);
            }
        }

        Matrix d = a;
        d -= b;

        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                ASSERT(d[row][col] == a[row][col] - b[row][col]);
            }
        }
    });

    TEST("Scalar multiplication", [] {
        Matrix a("../data/2a.txt");
        Matrix b = a * 7;

        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                ASSERT(b[row][col] == 7 * a[row][col]);
            }
        }

        Matrix c = a;
        c *= 7;

        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                ASSERT(c[row][col] == 7 * a[row][col]);
            }
        }
    });

    TEST("Matrix multiplication", [] {
        Matrix a("../data/2a.txt");
        Matrix b("../data/2b.txt");
        Matrix c = a * b;

        ASSERT(c.getRows() == a.getRows());
        ASSERT(c.getCols() == b.getCols());
        ASSERT(c[0][0] == 150);
        ASSERT(c[1][0] == 204);
        ASSERT(c[2][0] == 1);
    });

    TEST("Matrix transpose", [] {
        Matrix a("../data/2a.txt");
        Matrix a_t = ~a;

        ASSERT(a.getRows() == a_t.getCols());
        ASSERT(a.getCols() == a_t.getRows());

        for (int row = 0; row < a.getRows(); row++) {
            for (int col = 0; col < a.getCols(); col++) {
                ASSERT(a[row][col] == a_t[col][row]);
            }
        }

        Matrix a_t_t = ~a_t;
        ASSERT(a == a_t_t);

        Matrix b("../data/2b.txt");
        Matrix b_t = ~b;

        ASSERT(b.getRows() == b_t.getCols());
        ASSERT(b.getCols() == b_t.getRows());

        for (int row = 0; row < b.getRows(); row++) {
            for (int col = 0; col < b.getCols(); col++) {
                ASSERT(b[row][col] == b_t[col][row]);
            }
        }

        Matrix b_t_t = ~b_t;
        ASSERT(b == b_t_t);
    });

    TEST("Matrix inverse", [] {
        Matrix a(2, 2, {
                4, 7,
                2, 6
        });
        Matrix a_i = !a;
        Matrix expected_a_i(2, 2, {
                0.6, -0.7,
                -0.2, 0.4
        });
        Matrix e_2(2, 2, {
                1, 0,
                0, 1
        });

        ASSERT(a_i == expected_a_i);
        ASSERT(a * a_i == e_2);

        Matrix a_i_i = !a_i;
        ASSERT(a_i_i == a);

        Matrix b(3, 3, {
                0, 1, 2,
                1, 0, 3,
                4, -3, 8
        });
        Matrix b_i = !b;
        Matrix expected_b_i(3, 3, {
                -4.5, 7, -1.5,
                -2, 4, -1,
                1.5, -2, 0.5
        });
        Matrix e_3(3, 3, {
                1, 0, 0,
                0, 1, 0,
                0, 0, 1
        });

        ASSERT(b_i == expected_b_i);
        ASSERT(b * b_i == e_3);

        Matrix b_i_i = !b_i;
        ASSERT(b_i_i == b);

        Matrix c(3, 3, {
                4, -5, -2,
                5, -6, -2,
                -8, 9, 3
        });
        Matrix c_i = !c;
        Matrix expected_c_i(3, 3, {
                0, -3, -2,
                1, -4, -2,
                -3, 4, 1
        });

        ASSERT(c_i == expected_c_i);
        ASSERT(c * c_i == e_3);

        Matrix c_i_i = !c_i;
        ASSERT(c_i_i == c);
    });

    TEST("Forward substitution", [] {
        Matrix l(3, 3, {
                1, 0, 0,
                0.6, 1, 0,
                0.2, 0.571428571429, 1
        });
        Matrix b(3, 1, {10.3, 12.5, 0.1});
        Matrix y = l.ForwardSubstitution(b);
        Matrix expected_y(3, 1, {10.3, 6.32, -5.57142857143});

        ASSERT(y.getRows() == 3);
        ASSERT(y.getCols() == 1);
        ASSERT(y == expected_y);
    });

    TEST("Back substitution", [] {
        Matrix u(3, 3, {
                5, 6, 3,
                0, 1.4, 2.2,
                0, 0, -1.85714285714
        });
        Matrix y(3, 1, {10.3, 6.32, -5.57142857143});
        Matrix x = u.BackSubstitution(y);
        Matrix expected_x(3, 1, {0.5, -0.2, 3});

        ASSERT(x.getRows() == 3);
        ASSERT(x.getCols() == 1);
        ASSERT(x == expected_x);
    });

    TEST("LU decomposition", [] {
        Matrix a(4, 4, {
                2, 3, 1, 5,
                6, 13, 5, 19,
                2, 19, 10, 23,
                4, 10, 11, 31
        });
        Matrix lu = a.LuDecomposition();
        Matrix expected_lu(4, 4, {
                2, 3, 1, 5,
                3, 4, 2, 4,
                1, 4, 1, 2,
                2, 1, 7, 3
        });

        ASSERT(lu.getRows() == 4);
        ASSERT(lu.getCols() == 4);
        ASSERT(lu == expected_lu);
    });

    TEST("LUP decomposition", [] {
        Matrix a(3, 3, {
                1, 2, 0,
                3, 5, 4,
                5, 6, 3
        });
        std::vector<int> P;
        Matrix lu = a.LupDecomposition(P);
        Matrix expected_lu(3, 3, {
                5, 6, 3,
                0.6, 1.4, 2.2,
                0.2, 0.571428571429, -1.85714285714,
        });

        ASSERT(lu == expected_lu);
        ASSERT(P[0] == 2);
        ASSERT(P[1] == 1);
        ASSERT(P[2] == 0);

        a = Matrix(3, 3, {
                0, 1, 2,
                1, 0, 3,
                4, -3, 8
        });
        P = std::vector<int>();
        lu = a.LupDecomposition(P);
        expected_lu = Matrix(3, 3, {
                4, -3, 8,
                0, 1, 2,
                0.25, 0.75, -0.5
        });

        ASSERT(lu == expected_lu);
        ASSERT(P[0] == 2);
        ASSERT(P[1] == 0);
        ASSERT(P[2] == 1);
    });

    TEST("Determinant", [] {
        Matrix a(2, 2, {
            3, 8,
            4, 6
        });
        ASSERT(a.Determinant() == -14);

        a = Matrix(3, 3, {
                -2, 2, -3,
                -1, 1, 3,
                2, 0, -1
        });
        ASSERT(a.Determinant() == 18.0);
    });

    MyTester::runTests();
    return 0;
}
