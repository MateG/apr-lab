import sys
import subprocess
import matplotlib.pyplot as plt


def main():
    method_names = ["EULER", "BACKWARD EULER", "TRAPEZOIDAL", "RUNGE-KUTTA", "PE(CE)^2", "PECE"]
    method_outputs = []

    lines = subprocess.run(["./" + sys.argv[1]], stdout=subprocess.PIPE, text=True).stdout.splitlines()
    lines.append("eof")
    current_index = -1
    current_outputs = []
    for line in lines:
        if current_index < len(method_names) - 1 and line.startswith(method_names[current_index + 1]):
            current_index += 1
            if len(current_outputs) > 0:
                method_outputs.append(current_outputs)
                current_outputs = []
            continue

        if not line[0].isdigit():
            method_outputs.append(current_outputs)
            break

        line_parts = line.split(" ")
        current_outputs.append((line_parts[0], line_parts[1], line_parts[2]))

    fig, axs = plt.subplots(2, 3, figsize=(14, 6))
    plt.subplots_adjust(wspace=0.5, hspace=0.5)
    for i in range(len(method_outputs)):
        outputs = method_outputs[i]
        axs[int(i / 3), i % 3].plot(list(map(lambda x: float(x[0]), outputs)), list(map(lambda x: float(x[1]), outputs)))
        axs[int(i / 3), i % 3].plot(list(map(lambda x: float(x[0]), outputs)), list(map(lambda x: float(x[2]), outputs)))
        axs[int(i / 3), i % 3].set_title(method_names[i])
    plt.show()


if __name__ == '__main__':
    main()
