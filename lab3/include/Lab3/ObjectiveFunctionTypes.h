#ifndef OBJECTIVE_FUNCTION_TYPES_H
#define OBJECTIVE_FUNCTION_TYPES_H

#include <functional>
#include <vector>
#include <Lab1/Matrix.h>

namespace opt {

    /** Objective function's argument type. */
    typedef Matrix VectorArg;

    /** Simple (of a real variable) objective function type. */
    typedef std::function<double(double)> SimpleFunction;

    /** Objective function type. */
    typedef std::function<double(VectorArg&)> Function;

    /** Objective function's (1st or 2nd) derivation type. */
    typedef std::function<VectorArg(VectorArg&)> Derivation;
}

#endif //OBJECTIVE_FUNCTION_TYPES_H
