#ifndef BACKWARD_EULER_METHOD_H
#define BACKWARD_EULER_METHOD_H

#include "Lab5/IntegrationMethodsCommon.h"

std::vector<Matrix> backwardEuler(const Matrix& a, const Matrix& b, const RFunction& r,
                                  const Matrix& x0, double T, double t_max);

std::vector<Matrix> backwardEuler(const Matrix& a, const Matrix& x0, double T, double t_max);

#endif //BACKWARD_EULER_METHOD_H
