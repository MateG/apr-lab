#ifndef DECODER_H
#define DECODER_H

namespace opt {

    template<class C>
    class Decoder {

    public:
        std::vector<double>& Decode(C& chromosome) = 0;
    };

    template<>
    class Decoder<std::vector<double>> {

    public:
        std::vector<double>& Decode(std::vector<double>& chromosome) {
            return chromosome;
        }
    };

    template<>
    class Decoder<std::vector<unsigned long>> {

    private:

        int lower_;
        int upper_;
        int bit_count_;

        std::vector<double> decoded_;

    public:

        explicit Decoder(int lower, int upper, int bit_count)
                : lower_(lower),
                  upper_(upper),
                  bit_count_(bit_count) {}

        Decoder(Decoder<std::vector<unsigned long>>&& other) noexcept
                : lower_(other.lower_),
                  upper_(other.upper_),
                  bit_count_(other.bit_count_),
                  decoded_(std::move(other.decoded_)) {}

        std::vector<double>& Decode(std::vector<unsigned long>& chromosome) {
            int size = chromosome.size();
            if (decoded_.size() != size) {
                decoded_ = std::vector<double>(size);
            }

            for (int i = 0; i < size; i++) {
                decoded_[i] = bits::toDouble(chromosome[i], lower_, upper_, bit_count_);
            }

            return decoded_;
        }
    };
}

#endif //DECODER_H
