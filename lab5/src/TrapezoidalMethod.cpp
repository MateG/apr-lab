#include "Lab5/TrapezoidalMethod.h"

std::vector<Matrix> trapezoidal(const Matrix& a, const Matrix& b, const RFunction& r,
                                const Matrix& x0, const double T, const double t_max) {
    Matrix u = identityMatrix((int) a.getRows());
    IntegrationBiFunction calculate_next = [u, a, b, r, T](const Matrix& x, double t) {
        Matrix a_scaled = a * (T / 2.0);
        Matrix u_minus_a_scaled_inv = !(u - a_scaled);
        return u_minus_a_scaled_inv * (u + a_scaled) * x
               + u_minus_a_scaled_inv * (T / 2.0) * b * (r(t) + r(t + T));
    };
    return integrationMethod(calculate_next, x0, T, t_max);
}

std::vector<Matrix> trapezoidal(const Matrix& a, const Matrix& x0, const double T, const double t_max) {
    Matrix u = identityMatrix((int) a.getRows());
    IntegrationBiFunction calculate_next = [u, a, T](const Matrix& x, double t) {
        Matrix a_scaled = a * (T / 2.0);
        return !(u - a_scaled) * (u + a_scaled) * x;
    };
    return integrationMethod(calculate_next, x0, T, t_max);
}
