#ifndef RUNGE_KUTTA_METHOD_H
#define RUNGE_KUTTA_METHOD_H

#include "Lab5/IntegrationMethodsCommon.h"

std::vector<Matrix> fourthOrderRungeKutta(const Matrix& a, const Matrix& b, const RFunction& r,
                                          const Matrix& x0, double T, double t_max);

std::vector<Matrix> fourthOrderRungeKutta(const Matrix& a, const Matrix& x0, double T, double t_max);

#endif //RUNGE_KUTTA_METHOD_H
