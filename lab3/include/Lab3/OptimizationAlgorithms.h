#ifndef OPTIMIZATION_ALGORITHMS_H
#define OPTIMIZATION_ALGORITHMS_H

#define GOLDEN_RATIO 0.6180339887
#define DEFAULT_EPSILON 1e-6
#define DEFAULT_DX 0.5
#define DIVERGENCE_COUNT 100

#include <iostream>
#include <cmath>
#include <random>
#include "ObjectiveFunctionTypes.h"
#include "Util.h"
#include "Lab1/Matrix.h"

namespace opt {

    std::random_device rd; // NOLINT(cert-err58-cpp)
    std::mt19937 mt_engine(rd()); // NOLINT(cert-err58-cpp)
    std::uniform_real_distribution<> real_distribution(0.0, 1.0); // NOLINT(cert-err58-cpp)

    /** Boundaries of an interval */
    struct Interval {
        double a, b;
    };

    /**
     * Finds he specified function's unimodal interval.
     *
     * @tparam Callable Specified function type.
     * @param f Specified function.
     * @param x0 Starting point.
     * @param h Left/right initial step.
     * @return Found unimodal interval.
     */
    template<class Callable>
    Interval UnimodalInterval(Callable& f, double x0, double h) {
        unsigned int step = 1;

        double m = x0;
        double l = x0 - h;
        double r = x0 + h;

        double fm = f(m);
        double fl = f(l);
        double fr = f(r);

        if (!(fm < fr && fm < fl)) {
            if (fm > fr) {
                do {
                    l = m;
                    m = r;
                    fm = fr;
                    step *= 2;
                    r = x0 + h * step;
                    fr = f(r);
                } while (fm > fr);
            } else {
                do {
                    r = m;
                    m = l;
                    fm = fl;
                    step *= 2;
                    l = x0 - h * step;
                    fl = f(l);
                } while (fm > fl);
            }
        }

        return {l, r};
    }

    /**
     * Performs the golden section algorithm on the specified function.
     *
     * @tparam Callable Specified function type.
     * @param f Specified function.
     * @return Found point of minimum.
     */
    template<class Callable>
    double GoldenSectionSearch(Callable& f) {
        return GoldenSectionSearch(f, opt::UnimodalInterval(f, 0.0, 1.0));
    }

    /**
     * Performs the golden section algorithm on the specified function.
     *
     * @tparam Callable Specified function type.
     * @param f Specified function.
     * @param x0 Starting point (for unimodal interval search).
     * @param h Left/right initial step (for unimodal interval search).
     * @return Found point of minimum.
     */
    template<class Callable>
    double GoldenSectionSearch(Callable& f, double x0, double h) {
        return GoldenSectionSearch(f, opt::UnimodalInterval(f, x0, h));
    }

    /**
     * Performs the golden section algorithm on the specified function.
     *
     * @tparam Callable Specified function type.
     * @param f Specified function.
     * @param interval Specified interval.
     * @param epsilon Breaking condition value.
     * @return Found point of minimum.
     */
    template<class Callable>
    double GoldenSectionSearch(Callable& f, opt::Interval interval, double epsilon = DEFAULT_EPSILON) {
        double c = interval.b - GOLDEN_RATIO * (interval.b - interval.a);
        double d = interval.a + GOLDEN_RATIO * (interval.b - interval.a);
        double fc = f(c);
        double fd = f(d);

        while (true) {
            if (fc < fd) {
                interval.b = d;
                if (interval.b - interval.a <= epsilon) break;

                d = c;
                c = interval.b - GOLDEN_RATIO * (interval.b - interval.a);
                fd = fc;
                fc = f(c);
            } else {
                interval.a = c;
                if (interval.b - interval.a <= epsilon) break;

                c = d;
                d = interval.a + GOLDEN_RATIO * (interval.b - interval.a);
                fc = fd;
                fd = f(d);
            }
        }

        return (interval.a + interval.b) / 2.0;
    }

    /**
     * Performs the Hooke-Jeeves algorithm on the specified function.
     *
     * @tparam Function Specified function type.
     * @param f Specified function.
     * @param x0 Starting point.
     * @param dx Movement vector.
     * @param epsilon Breaking condition vector.
     * @return Found point of minimum.
     */
    template<class Function>
    Matrix HookeJeevesMethod(Function& f, const Matrix& x0, Matrix& dx, const Matrix& epsilon) {
        Matrix xp = x0;
        Matrix xb = x0;
        int size = (int) x0.getSize();

        while (true) {
            Matrix xn = xp;
            double n = 0.0;
            for (int i = 0; i < size; i++) {
                double p = f(xn);
                xn[i][0] += dx[i][0];
                n = f(xn);

                if (n > p) {
                    xn[i][0] -= 2 * dx[i][0];
                    n = f(xn);
                    if (n > p) {
                        xn[i][0] += dx[i][0];
                        if (i == size - 1) n = f(xn);
                    }
                }
            }

            if (n < f(xb)) {
                for (int i = 0; i < size; i++) {
                    xp[i][0] = 2 * xn[i][0] - xb[i][0];
                }
                xb = xn;
            } else {
                dx *= 0.5;
                xp = xb;
            }

            bool end_reached = true;
            for (int i = 0; i < size; i++) {
                if (dx[i][0] > epsilon.Get(i)) {
                    end_reached = false;
                    break;
                }
            }
            if (end_reached) break;
        }

        return xb;
    }

    /**
     * Performs the Hooke-Jeeves algorithm on the specified function.
     *
     * @tparam Function Specified function type.
     * @param f Specified function.
     * @param x0 Starting point.
     * @return Found point of minimum.
     */
    template<class Function>
    Matrix HookeJeevesMethod(Function& f, const Matrix& x0) {
        unsigned int n = x0.getSize();

        Matrix dx(n, 1, false);
        dx.Clear(DEFAULT_DX);

        Matrix epsilon(n, 1, false);
        epsilon.Clear(DEFAULT_EPSILON);

        return HookeJeevesMethod(f, x0, dx, epsilon);
    }

    /**
     * Performs the gradient descent algorithm on the specified function.
     *
     * @tparam Function Specified function type.
     * @tparam Derivation Specified derivation type.
     * @param f Specified function.
     * @param f_gradient Specified gradient function.
     * @param x0 Specified starting point.
     * @param golden_section Flag for golden section usage in movement calculation.
     * @param epsilon Breaking condition value.
     * @return Found point of minimum.
     */
    template<class Function, class Derivation>
    Matrix GradientDescent(Function& f, Derivation& f_gradient, const Matrix& x0,
                           bool golden_section = false, double epsilon = DEFAULT_EPSILON) {
        Matrix x = x0;
        double fx = f(x);
        int counter = 0;

        while (counter < DIVERGENCE_COUNT) {
            Matrix gradient = f_gradient(x);
            if (gradient.Norm() < epsilon) return x;

            double factor;
            if (golden_section) {
                SimpleFunction simple_f = [&](double factor) mutable {
                    Matrix x_copy = x;
                    x_copy += gradient * factor;
                    return f(x_copy);
                };
                factor = GoldenSectionSearch(simple_f);
            } else {
                factor = -1.0;
            }

            x += gradient * factor;

            double new_fx = f(x);
            if (new_fx < fx) {
                counter = 0;
                fx = new_fx;
            } else {
                counter++;
            }
        }

        throw std::runtime_error("Divergence detected!");
    }

    /**
     * Performs the Newton-Raphson method on the specified function.
     *
     * @tparam Function Specified function type.
     * @tparam Derivation Specified derivation type.
     * @param f Specified function.
     * @param f_gradient Specified gradient function.
     * @param f_hessian Specified Hessian function.
     * @param x0 Specified starting point.
     * @param golden_section Flag for golden section usage in movement calculation.
     * @param epsilon Breaking condition value.
     * @return Found point of minimum.
     */
    template<class Function, class Derivation>
    Matrix NewtonRaphsonMethod(Function& f, Derivation& f_gradient, Derivation& f_hessian, const Matrix& x0,
                               bool golden_section = false, double epsilon = DEFAULT_EPSILON) {
        Matrix x = x0;
        double fx = f(x);
        int counter = 0;

        while (counter < DIVERGENCE_COUNT) {
            Matrix gradient = f_gradient(x);
            Matrix hessian = f_hessian(x);
            Matrix delta = !hessian * gradient;

            double factor;
            if (golden_section) {
                SimpleFunction simple_f = [&](double factor) mutable {
                    Matrix x_copy = x;
                    x_copy += delta * factor;
                    return f(x_copy);
                };
                factor = GoldenSectionSearch(simple_f);
            } else {
                factor = -1.0;
            }

            delta *= factor;
            if (delta.Norm() < epsilon) return x;

            x += delta;

            double new_fx = f(x);
            if (new_fx < fx) {
                counter = 0;
                fx = new_fx;
            } else {
                counter++;
            }
        }

        throw std::runtime_error("Divergence detected!");
    }

    /** Predicate type for implicit limitations. */
    typedef std::function<bool(const Matrix&)> ImplicitLimitation;

    /**
     * Performs the Box method on the specified function.
     *
     * @tparam Function Specified function type.
     * @param f Specified function.
     * @param implicit_limitations Collection of implicit limitation predicates.
     * @param x0 Specified starting point.
     * @param d Specified lower bound value.
     * @param g Specified upper bound value.
     * @param alpha Specified reflection factor.
     * @param epsilon Breaking condition value.
     * @return Found point of minimum.
     */
    template<class Function>
    Matrix BoxMethod(Function& f, const std::vector<ImplicitLimitation>& implicit_limitations, const Matrix& x0,
                     double d, double g, double alpha = 1.3, double epsilon = 1e-6) {
        Matrix xd(x0.getRows(), x0.getCols(), false);
        xd.Clear(d);
        Matrix xg(x0.getRows(), x0.getCols(), false);
        xg.Clear(g);
        return BoxMethod(f, implicit_limitations, x0, xd, xg, alpha, epsilon);
    }

    /**
     * Performs the Box method on the specified function.
     *
     * @tparam Function Specified function type.
     * @param f Specified function.
     * @param implicit_limitations Collection of implicit limitation predicates.
     * @param x0 Specified starting point.
     * @param xd Specified lower bound vector.
     * @param xg Specified upper bound vector.
     * @param alpha Specified reflection factor.
     * @param epsilon Breaking condition value.
     * @return Found point of minimum.
     */
    template<class Function>
    Matrix BoxMethod(Function& f, const std::vector<ImplicitLimitation>& implicit_limitations, const Matrix& x0,
                     const Matrix& xd, const Matrix& xg, double alpha = 1.3, double epsilon = 1e-6) {
        int n = (int) x0.getRows();
        for (int i = 0; i < n; i++) {
            double x0_i = x0.Get(i);
            if (x0_i < xd.Get(i) || x0_i > xg.Get(i)) {
                throw std::runtime_error("x0 is out of defined boundaries!");
            }
        }

        auto implicit_limitations_valid = [implicit_limitations](const Matrix& x) mutable {
            for (auto& limitation : implicit_limitations) {
                if (!limitation(x)) return false;
            }
            return true;
        };

        if (!implicit_limitations_valid(x0)) {
            throw std::runtime_error("x0 does not meet the implicit limitations!");
        }

        int k = 2 * n; // recommended by Box
        std::vector<Matrix> x;
        x.reserve(k);
        Matrix xc = x0;
        x.push_back(x0);

        // Initialize helper lambdas
        auto end_reached = [k, f, epsilon](std::vector<Matrix>& x, Matrix& xc) mutable {
            double fc = f(xc);
            double value = 0.0;
            for (int i = 0; i < k; i++) {
                double f_i = f(x[i]);
                value += std::pow(f_i - fc, 2);
            }
            value /= k;
            return std::sqrt(value) <= epsilon;
        };
        auto reflection = [n, alpha](Matrix& xc, Matrix& xh) {
            return xc * (1 + alpha) - xh * alpha;
        };
        auto h_index = [f](std::vector<Matrix>& x, int k) mutable {
            int h = 0;
            double max = f(x[0]);
            for (int i = 1; i < k; i++) {
                double f_i = f(x[i]);
                if (f_i > max) {
                    h = i;
                    max = f_i;
                }
            }
            return h;
        };
        auto h2_index = [f, k](std::vector<Matrix>& x, int h) mutable {
            int h2 = h == 0 ? 1 : 0;
            double max2 = f(x[h2]);
            for (int i = h2 + 1; i < k; i++) {
                if (i == h) continue;

                double f_i = f(x[i]);
                if (f_i > max2) {
                    h2 = i;
                    max2 = f_i;
                }
            }
            return h2;
        };

        for (int t = 1; t < k; t++) {
            Matrix xt(n, 1, false);
            for (int i = 0; i < n; i++) {
                xt[i][0] = xd.Get(i) + (xg.Get(i) - xd.Get(i)) * real_distribution(mt_engine);
            }
            while (!implicit_limitations_valid(xt)) {
                xt = (xt + xc) * 0.5;
            }
            x.push_back(xt);

            // Update the centroid
            int h = h_index(x, t + 1);
            xc.Clear();
            for (int i = 0; i <= t; i++) {
                if (i == h) continue;
                xc += x[i];
            }
            xc *= 1.0 / t;
        }

        double fc = f(xc);
        int counter = 0;
        while (counter < DIVERGENCE_COUNT * 100) {
            int h = h_index(x, k);
            Matrix& xh = x[h];

            int h2 = h2_index(x, h);
            Matrix& xh2 = x[h2];

            // Calculate the centroid without xh
            xc.Clear();
            for (int t = 0; t < k; t++) {
                if (t == h) continue;
                xc += x[t];
            }
            xc *= 1.0 / (k - 1);

            double new_fc = f(xc);
            if (new_fc < fc) {
                counter = 0;
                fc = new_fc;
            } else {
                counter++;
            }

            Matrix xr = reflection(xc, xh);
            for (int i = 0; i < n; i++) {
                double xr_i = xr.Get(i);
                if (xr_i < xd.Get(i)) xr[i][0] = xd.Get(i);
                else if (xr_i > xg.Get(i)) xr[i][0] = xg.Get(i);
            }

            int implicit_counter = 0;
            while (!implicit_limitations_valid(xr)) {
                xr = (xr + xc) * 0.5;
                implicit_counter++;
                if (implicit_counter >= DIVERGENCE_COUNT) return xc;
            }

            if (f(xr) > f(xh2)) {
                xr = (xr + xc) * 0.5;
            }

            x[h] = xr;

            if (end_reached(x, xc)) break;
        }

        return xc;
    }

    /**
     * Performs a transformation to a problem without limitations and finds the minimum using the Hooke-Jeeves method.
     *
     * @tparam Function Specified function type.
     * @tparam Limitation Specified limitation type.
     * @param f Specified function.
     * @param g_functions Specified limitations: g_i(x) >= 0.
     * @param h_functions Specified limitations: h_i(x) == 0.
     * @param x0 Specified starting point.
     * @param t Specified transformation factor.
     * @param epsilon Breaking condition value.
     * @return Found point of minimum.
     */
    template<class Function, class Limitation>
    Matrix NoLimitationTransformation(Function& f, std::vector<Limitation> g_functions,
                                      std::vector<Limitation> h_functions,
                                      Matrix& x0, double t = 1.0, double epsilon = 1e-6) {
        auto inside_g_boundaries = [&](Matrix& x0) mutable {
            for (auto& g : g_functions) {
                if (g(x0) < 0.0) return false;
            }
            return true;
        };
        auto find_inner = [&](Matrix& x0) mutable {
            opt::Function big_g = [&](Matrix& x) {
                double sum = 0.0;
                for (auto& g : g_functions) {
                    double g_x = g(x);
                    if (g_x >= 0.0) continue;
                    sum += t * g_x;
                }
                return -sum;
            };
            Matrix inner_x = HookeJeevesMethod(big_g, x0);
            std::cout << "Found inner x0 =\n" << inner_x;
            return inner_x;
        };

        Matrix x = inside_g_boundaries(x0) ? x0 : find_inner(x0);

        opt::Function transformed_function = [&](Matrix& x) mutable {
            double g_sum = 0.0;
            for (auto& g : g_functions) {
                double g_x = g(x);
                if (g_x <= 0.0) return std::numeric_limits<double>::infinity();
                g_sum += log(g_x);
            }
            double h_sum = 0.0;
            for (auto& h : h_functions) h_sum += pow(h(x), 2.0);
            return f(x) - 1.0 / t * g_sum + t * h_sum;
        };
        auto end_reached = [epsilon](Matrix& x, Matrix& new_x) mutable {
            for (int i = 0; i < (int) x.getSize(); i++) {
                if (std::abs(x[i][0] - new_x[i][0]) > epsilon) return false;
            }
            return true;
        };

        while (true) {
            Matrix new_x = HookeJeevesMethod(transformed_function, x);
            if (end_reached(x, new_x)) return new_x;

            x = new_x;
            t *= 10.0;
        }
    }
}


#endif //OPTIMIZATION_ALGORITHMS_H
