#include <iostream>
#include <iomanip>
#include "TasksUtil.h"

int main() {
    std::cout << std::setprecision(16);

    std::cout << "f6 [1D]" << "\n";
    runDecimal(opt::f6, 1, 20, 3, 0.1);
    std::cout << "\nf6 [3D]" << "\n";
    runDecimal(opt::f6, 3, 30, 3, 0.1);
    std::cout << "\nf6 [6D]" << "\n";
    runDecimal(opt::f6, 6, 30, 3, 0.1);
    std::cout << "\nf6 [10D]" << "\n";
    runDecimal(opt::f6, 10, 40, 3, 0.1);

    std::cout << "\nf7 [1D]" << "\n";
    runDecimal(opt::f7, 1, 30, 3, 0.1);
    std::cout << "\nf7 [3D]" << "\n";
    runDecimal(opt::f7, 3, 50, 3, 0.3);
    std::cout << "\nf7 [6D]" << "\n";
    runDecimal(opt::f7, 6, 150, 3, 0.5);
    std::cout << "\nf7 [10D]" << "\n";
    runDecimal(opt::f7, 10, 200, 3, 0.5);

    return 0;
}
