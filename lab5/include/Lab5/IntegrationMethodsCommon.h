#ifndef INTEGRATION_METHODS_COMMON_H
#define INTEGRATION_METHODS_COMMON_H

#include <functional>
#include <vector>
#include "Lab1/Matrix.h"

typedef std::function<Matrix(const Matrix&, double)> IntegrationBiFunction;

std::vector<Matrix> integrationMethod(const IntegrationBiFunction& calculate_next,
                                      const Matrix& x0, double T, double t_max);

typedef std::function<Matrix(double)> RFunction;

Matrix identityMatrix(int rows);

Matrix x_derivation(const Matrix& x, double t,
                    const Matrix& a, const Matrix& b, const RFunction& r);

#endif //INTEGRATION_METHODS_COMMON_H
