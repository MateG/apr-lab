#ifndef MY_TESTER_MACRO_H
#define MY_TESTER_MACRO_H

#include "Core.h"

#define TEST(name, test) \
    MyTester::addTest(name, test)

#define ASSERT(expression) \
    MyTester::assertTrue(expression)

#endif //MY_TESTER_MACRO_H
