#include "Lab5/RungeKuttaMethod.h"

std::vector<Matrix> fourthOrderRungeKutta(const Matrix& a, const Matrix& b, const RFunction& r,
                                          const Matrix& x0, const double T, const double t_max) {
    IntegrationBiFunction calculate_next = [a, b, r, T](const Matrix& x, double t) {
        Matrix m1 = a * x + b * r(t);
        Matrix m2 = a * (x + m1 * (T / 2.0)) + b * r(t + T / 2.0);
        Matrix m3 = a * (x + m2 * (T / 2.0)) + b * r(t + T / 2.0);
        Matrix m4 = a * (x + m3 * T) + b * r(t + T);
        return x + (m1 + m2 * 2 + m3 * 2 + m4) * (T / 6.0);
    };
    return integrationMethod(calculate_next, x0, T, t_max);
}

std::vector<Matrix> fourthOrderRungeKutta(const Matrix& a, const Matrix& x0, const double T, const double t_max) {
    Matrix b(a.getRows(), a.getCols());
    Matrix r_zero(x0.getRows(), x0.getCols());
    std::function<Matrix(double)> r = [r_zero](double t) {
        return r_zero;
    };
    return fourthOrderRungeKutta(a, b, r, x0, T, t_max);
}
