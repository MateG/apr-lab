#ifndef BIT_UTIL_H
#define BIT_UTIL_H

namespace bits {

    unsigned int getBit(unsigned long bits, unsigned int index) {
        return (bits >> index) & 1ul;
    }

    void setBit(unsigned long& bits, unsigned int index, unsigned int value) {
        bits ^= (-value ^ bits) & (1ul << index);
    }

    void invertBit(unsigned long& bits, unsigned int index) {
        setBit(bits, index, 1u ^ getBit(bits, index));
    }

    double toDouble(unsigned long bits, double lower, double upper, int bit_count) {
        return lower + (upper - lower) * bits / ((1u << unsigned(bit_count)) - 1);
    }
}

#endif //BIT_UTIL_H
