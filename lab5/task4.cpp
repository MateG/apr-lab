#include "Lab5/IntegrationUtil.h"
#include "Lab5/EulerMethod.h"
#include "Lab5/BackwardEulerMethod.h"
#include "Lab5/TrapezoidalMethod.h"
#include "Lab5/RungeKuttaMethod.h"
#include "Lab5/PredictorCorrectorMethod.h"

int main() {
    const double T = 0.01;
    const double t_max = 1.0;
    const int log_period = 1;

    Matrix a("resources/4_a.txt");
    Matrix x0("resources/4_x0.txt");
    Matrix b("resources/4_b.txt");
    RFunction r = [](double t) {
        return Matrix(2, 1, {t, t});
    };

    std::cout << "EULER:\n";
    printSteps(euler(a, b, r, x0, T, t_max), T, log_period);

    std::cout << "BACKWARD EULER:\n";
    printSteps(backwardEuler(a, b, r, x0, T, t_max), T, log_period);

    std::cout << "TRAPEZOIDAL:\n";
    printSteps(trapezoidal(a, b, r, x0, T, t_max), T, log_period);

    std::cout << "RUNGE-KUTTA:\n";
    printSteps(fourthOrderRungeKutta(a, b, r, x0, T, t_max), T, log_period);

    std::cout << "PE(CE)^2:\n";
    printSteps(eulerBackwardEulerPECE2(a, b, r, x0, T, t_max), T, log_period);

    std::cout << "PECE:\n";
    printSteps(eulerTrapezoidalPECE(a, b, r, x0, T, t_max), T, log_period);

    return 0;
}
