#ifndef MATRIX_H
#define MATRIX_H

#include <string>
#include <vector>

/**
 * Represents a matrix of M rows and N columns and contains implementations
 * of various mathematical operations.
 *
 * @author Mate Gašparini
 */
class Matrix {

private:

    /** Row count. */
    unsigned rows_;

    /** Column count. */
    unsigned cols_;

    /** Element count (cached to avoid multiplication). */
    unsigned size_;

    /** Pointer to the heap memory containing matrix data. */
    double* data_;

public:

    /**
     * Constructor which specifies the matrix dimensions and initializes its
     * data with zeros if specified.
     *
     * @param rows Row count.
     * @param cols Column count.
     * @param zero_init If true, data is initialized with zeros.
     */
    Matrix(unsigned rows, unsigned cols, bool zero_init = true);

    /**
     * Constructor which loads the matrix from the specified file.
     *
     * @param file_path Path to the specified file.
     */
    explicit Matrix(const std::string& file_path);

    /**
     * Copy constructor.
     *
     * @param original Reference to the original matrix.
     */
    Matrix(const Matrix& original);

    /**
     * Constructor which specifies the matrix dimensions and initializes its
     * data with the values specified in the initializer list.
     *
     * @param rows Row count.
     * @param cols Column count.
     * @param values Specified initial values.
     */
    Matrix(unsigned rows, unsigned cols, std::initializer_list<double> values);

    /**
     * Destructor which deallocates the matrix data.
     */
    ~Matrix();

    /** @return Row count. */
    unsigned getRows() const;

    /** @return Column count. */
    unsigned getCols() const;

    /** @return Element count. */
    unsigned getSize() const;

    /**
     * @param row Specified row.
     * @param col Specified column.
     * @return Corresponding element.
     */
    double Get(int row, int col = 0) const;

    /**
     * Clears the matrix with the given value.
     *
     * @param value Given value.
     */
    void Clear(double value = 0.0);

    /**
     * Assigns the specified matrix to this matrix. If not self-assigned,
     * it is reallocated (if necessary) and its data is copied.
     *
     * @param other Specified matrix.
     * @return This matrix.
     */
    Matrix& operator=(const Matrix& other);

    /**
     * Returns the row at the specified index.
     *
     * @param row Specified row index.
     * @return Pointer to the beginning of the specified row.
     */
    double* operator[](unsigned row);

    /**
     * Calculates the sum matrix of this and the specified matrix elements.
     *
     * @param other Specified matrix.
     * @return Calculated sum matrix.
     */
    Matrix operator+(const Matrix& other) const;

    /**
     * Adds the specified matrix elements to this matrix elements.
     *
     * @param other Specified matrix.
     */
    void operator+=(const Matrix& other);

    /**
     * Calculates the difference matrix of this and the specified matrix elements.
     *
     * @param other Specified matrix.
     * @return Calculated difference matrix.
     */
    Matrix operator-(const Matrix& other) const;

    /**
     * Subtracts the specified matrix elements from this matrix elements.
     *
     * @param other Specified matrix.
     */
    void operator-=(const Matrix& other);

    /**
     * Calculates the product matrix of this matrix and the specified scalar.
     *
     * @param scalar Specified scalar.
     * @return Calculated product matrix.
     */
    Matrix operator*(double scalar) const;

    /**
     * Multiplies this matrix with the specified scalar.
     *
     * @param scalar Specified scalar.
     */
    void operator*=(double scalar);

    /**
     * Calculates the product matrix of this matrix and the specified matrix.
     *
     * @param other Specified matrix.
     * @return Calculated product matrix.
     */
    Matrix operator*(const Matrix& other) const;

    /**
     * Calculates the transpose of this matrix.
     *
     * @return Calculated transposed matrix.
     */
    Matrix operator~() const;

    /**
     * Calculates the inverse of this matrix.
     *
     * @return Calculated matrix inverse.
     */
    Matrix operator!() const;

    /**
     * Tests the equality of this and the specified matrix.
     *
     * @param other Specified matrix.
     * @return True if the matrices are equal, false otherwise.
     */
    bool operator==(const Matrix& other) const;

    /**
     * Tests the inequality of this and the specified matrix.
     *
     * @param other Specified matrix.
     * @return False if the matrices are equal, true otherwise.
     */
    bool operator!=(const Matrix& other) const;

    /**
     * Performs forward substitution for solving the equation:
     * L*y=b, where L is the current (lower triangular) matrix,
     * b is the specified free vector and y is the solution vector.
     *
     * @param vector Specified free vector.
     * @return Solution vector.
     */
    Matrix ForwardSubstitution(const Matrix& vector) const;

    /**
     * Performs back substitution for solving the equation:
     * U*x=y, where U is the current (upper triangular) matrix,
     * y is the specified free vector and x is the solution vector.
     *
     * @param vector Specified free vector.
     * @return Solution vector.
     */
    Matrix BackSubstitution(const Matrix& vector) const;

    /**
     * Performs LU decomposition on a copy of this matrix.
     *
     * @return LU decomposition of this matrix.
     */
    Matrix LuDecomposition() const;

    /**
     * Performs LU decomposition on this matrix.
     */
    void LuDecompose();

    /**
     * Performs LUP decomposition on a copy of this matrix with P vector
     * filled with permutation indexes/count.
     *
     * @param P Empty vector allocated by the caller.
     * @return LUP decomposition of this matrix.
     */
    Matrix LupDecomposition(std::vector<int>& P) const;

    /**
     * Performs LUP decomposition on this matrix with P vector
     * filled with permutation indexes/count.
     *
     * @param P Empty vector allocated by the caller.
     */
    void LupDecompose(std::vector<int>& P);

    /**
     * Calculates the determinant of this matrix using LUP decomposition.
     *
     * @return Calculated determinant value.
     */
    double Determinant() const;

    /**
     * Permutes a copy of this matrix using the specified permutation vector
     * which contains the permutation indexes.
     *
     * @param P Specified permutation vector.
     * @return Permutation of this matrix.
     */
    Matrix Permutation(const std::vector<int>& P);

    /**
     * Permutes this matrix using the specified permutation vector
     * which contains the permutation indexes.
     *
     * @param P Specified permutation vector.
     */
    void Permute(const std::vector<int>& P);

    /**
     * Returns the norm of the underlying vector.
     *
     * @return Calculated norm.
     */
    double Norm() const;

    /**
     * Writes the specified matrix data to the specified stream.
     *
     * @param stream Specified stream.
     * @param matrix Specified matrix.
     * @return Specified stream.
     */
    friend std::ostream& operator<<(std::ostream& stream, const Matrix& matrix);

private:

    /**
     * Allocates data on the heap using the specified dimensions.
     *
     * @param rows Row count.
     * @param cols Column count.
     */
    void allocateData(unsigned rows, unsigned cols);

    /**
     * Deallocates the allocated data off the heap.
     */
    void deallocateData();

    /**
     * Performs unchecked LUP decomposition, including permutation of LU.
     *
     * @param P Empty vector allocated by the caller.
     */
    void lup(std::vector<int>& P);

    /**
     * Replaces the specified column of this matrix with the specified vector.
     *
     * @param vector Specified vector.
     * @param col Index of the specified column.
     */
    void setVector(const Matrix& vector, int col);
};


#endif //MATRIX_H
