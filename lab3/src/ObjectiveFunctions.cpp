#include "Lab3/ObjectiveFunctions.h"

#include <cmath>
#include <Lab1/Matrix.h>

using namespace opt;

Function opt::f1 = [](VectorArg& x) { // NOLINT(cert-err58-cpp)
    double x1 = x[0][0];
    double x2 = x[1][0];

    return 100.0 * std::pow(x2 - x1 * x1, 2) + std::pow(1.0 - x1, 2);
};

Derivation opt::f1_gradient = [](VectorArg& x) { // NOLINT(cert-err58-cpp)
    double x1 = x[0][0];
    double x2 = x[1][0];

    return Matrix(2, 1, {
            -400 * x1 * (x2 - x1 * x1) - 2 * (1 - x1),
            200 * (x2 - x1 * x1)
    });
};

Derivation opt::f1_hessian = [](VectorArg& x) { // NOLINT(cert-err58-cpp)
    double x1 = x[0][0];
    double x2 = x[1][0];

    return Matrix(2, 2, {
            -400 * (x2 - 3 * x1 * x1) + 2, -400 * x1,
            -400 * x1, 200
    });
};

Function opt::f2 = [](VectorArg& x) { // NOLINT(cert-err58-cpp)
    double x1 = x[0][0];
    double x2 = x[1][0];

    return std::pow(x1 - 4.0, 2) + 4.0 * std::pow(x2 - 2.0, 2);
};

Derivation opt::f2_gradient = [](VectorArg& x) { // NOLINT(cert-err58-cpp)
    double x1 = x[0][0];
    double x2 = x[1][0];

    return Matrix(2, 1, {
            2 * (x1 - 4),
            8 * (x2 - 2)
    });
};

Derivation opt::f2_hessian = [](VectorArg& x) { // NOLINT(cert-err58-cpp)
    return Matrix(2, 2, {
            2, 0,
            0, 8
    });
};

Function opt::f3 = [](VectorArg& x) { // NOLINT(cert-err58-cpp)
    double x1 = x[0][0];
    double x2 = x[1][0];

    return std::pow(x1 - 2.0, 2) + std::pow(x2 + 3.0, 2);
};

Derivation opt::f3_gradient = [](VectorArg& x) { // NOLINT(cert-err58-cpp)
    double x1 = x[0][0];
    double x2 = x[1][0];

    return Matrix(2, 1, {
            2 * (x1 - 2),
            2 * (x2 + 3)
    });
};

Function opt::f4 = [](VectorArg& x) { // NOLINT(cert-err58-cpp)
    double x1 = x[0][0];
    double x2 = x[1][0];

    return std::pow(x1 - 3.0, 2) + std::pow(x2, 2);
};
