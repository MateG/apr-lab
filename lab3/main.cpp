#include <iostream>
#include "Lab3/OptimizationAlgorithms.h"
#include "Lab3/ObjectiveFunctions.h"

using namespace opt;

typedef TrackedFunction<double, Matrix&> TF;
typedef TrackedFunction<Matrix, Matrix&> TD;

void handleRuntimeError(const std::runtime_error& e) {
    std::cout << "FAILURE: " << e.what() << "\n";
}

void printStatistics(Matrix& x_min, TF& t) {
    std::cout << "SUCCESS:\n\nx_min =\n" << x_min;
    std::cout << "f(x_min) = " << t(x_min) << "\n\n";
    std::cout << "f called " << t.getCallCount() - 1 << " times\n";
}

void printStatistics(Matrix& x_min, TF& t, TD& t_gradient) {
    printStatistics(x_min, t);
    std::cout << "f_gradient called " << t_gradient.getCallCount() << " times\n";
}

void printStatistics(Matrix& x_min, TF& t, TD& t_gradient, TD& t_hessian) {
    printStatistics(x_min, t, t_gradient);
    std::cout << "f_hessian called " << t_hessian.getCallCount() << " times\n";
}

void tryGradientDescent(Function& f, Derivation& f_gradient, const Matrix& x0, bool golden_section = false) {
    try {
        std::cout << "\nTrying gradient descent" << (golden_section ? " with golden section" : "") << "...\n";
        TF t(f);
        TD t_gradient(f_gradient);
        Matrix x_min = GradientDescent(t, t_gradient, x0, golden_section);
        printStatistics(x_min, t, t_gradient);
    } catch (std::runtime_error& e) {
        handleRuntimeError(e);
    }
}

void tryNewtonRaphson(Function& f, Derivation& f_gradient, Derivation& f_hessian, const Matrix& x0,
                      bool golden_section = false) {
    try {
        std::cout << "\nTrying Newton-Raphson" << (golden_section ? " with golden section" : "") << "...\n";
        TF t(f);
        TD t_gradient(f_gradient);
        TD t_hessian(f_hessian);
        Matrix x_min = NewtonRaphsonMethod(t, t_gradient, t_hessian, x0, golden_section);
        printStatistics(x_min, t, t_gradient, t_hessian);
    } catch (std::runtime_error& e) {
        handleRuntimeError(e);
    }
}

void tryBox(Function& f, const Matrix& x0, std::vector<ImplicitLimitation>& implicit_limitations, double d, double g) {
    try {
        std::cout << "\nTrying Box method...\n";
        TF t(f);
        Matrix x_min = BoxMethod(t, implicit_limitations, x0, d, g);
        printStatistics(x_min, t);
    } catch (std::runtime_error& e) {
        handleRuntimeError(e);
    }
}

void noLimitTransform(Function& f, Matrix& x0, std::vector<Function>& g_functions, std::vector<Function>& h_functions) {
    std::cout << "Transforming to a no limit problem...\n";
    TF t(f);
    Matrix x_min = NoLimitationTransformation(t, g_functions, h_functions, x0);
    printStatistics(x_min, t);
}

void s1() {
    std::cout << "\nf(x) = (x1 - 2) ^ 2 + (x2 + 3) ^ 2\n";
    Matrix x0(2, 1, {0.0, 0.0});
    std::cout << "\nx0 =\n" << x0;

    tryGradientDescent(f3, f3_gradient, x0);
    tryGradientDescent(f3, f3_gradient, x0, true);
}

void s2() {
    std::cout << "\nf(x) = 100 * (x2 - x1 ^ 2) ^ 2 + (1 - x1) ^ 2\n";
    Matrix x0(2, 1, {-1.9, 2.0});
    std::cout << "\nx0 =\n" << x0;

    tryGradientDescent(f1, f1_gradient, x0, true);
    tryNewtonRaphson(f1, f1_gradient, f1_hessian, x0, true);

    std::cout << "\nf(x) = (x1 - 4) ^ 2 + 4 * (x2 - 2) ^ 2\n";
    x0 = Matrix(2, 1, {0.1, 0.3});
    std::cout << "\nx0 =\n" << x0;

    tryGradientDescent(f2, f2_gradient, x0, true);
    tryNewtonRaphson(f2, f2_gradient, f2_hessian, x0, true);
}

void s3() {
    std::vector<ImplicitLimitation> implicit_limitations = {
            [](const Matrix& x) { return x.Get(1) - x.Get(0) >= 0.0; },
            [](const Matrix& x) { return 2.0 - x.Get(0) >= 0.0; }
    };
    double d = -100;
    double g = 100;

    std::cout << "\nf(x) = 100 * (x2 - x1 ^ 2) ^ 2 + (1 - x1) ^ 2\n";
    Matrix x0(2, 1, {-1.9, 2.0});
    std::cout << "\nx0 =\n" << x0;

    tryBox(f1, x0, implicit_limitations, d, g);

    x0 = Matrix(2, 1, {0.0, 2.0});
    std::cout << "\nx0 =\n" << x0;

    tryBox(f1, x0, implicit_limitations, d, g);

    std::cout << "\nf(x) = (x1 - 4) ^ 2 + 4 * (x2 - 2) ^ 2\n";
    x0 = Matrix(2, 1, {0.1, 0.3});
    std::cout << "\nx0 =\n" << x0;

    tryBox(f2, x0, implicit_limitations, d, g);
}

void s4() {
    std::vector<Function> g_functions = {
            [](const Matrix& x) { return x.Get(1) - x.Get(0); },
            [](const Matrix& x) { return 2.0 - x.Get(0); }
    };
    std::vector<Function> h_functions = {};

    std::cout << "\nf(x) = 100 * (x2 - x1 ^ 2) ^ 2 + (1 - x1) ^ 2\n";
    Matrix x0(2, 1, {-1.9, 2.0});
    std::cout << "\nx0 =\n" << x0;

    noLimitTransform(f1, x0, g_functions, h_functions);

    std::cout << "\nf(x) = (x1 - 4) ^ 2 + 4 * (x2 - 2) ^ 2\n";
    x0 = Matrix(2, 1, {0.1, 0.3});
    std::cout << "\nx0 =\n" << x0;

    noLimitTransform(f2, x0, g_functions, h_functions);
}

void s5() {
    std::vector<Function> g_functions = {
            [](const Matrix& x) { return 3.0 - x.Get(0) - x.Get(1); },
            [](const Matrix& x) { return 3.0 + 1.5 * x.Get(0) - x.Get(1); }
    };
    std::vector<Function> h_functions = {
            [](const Matrix& x) { return x.Get(1) - 1.0; }
    };

    std::cout << "\nf(x) = (x1 - 3) ^ 2 + x2 ^ 2\n";
    Matrix x0(2, 1, {0.0, 0.0});
    std::cout << "\nx0 =\n" << x0;

    noLimitTransform(f4, x0, g_functions, h_functions);

    x0 = Matrix(2, 1, {5.0, 5.0});
    std::cout << "\nx0 =\n" << x0;

    noLimitTransform(f4, x0, g_functions, h_functions);
}

int main() {
    void (* solutions[])() = {
            s1, s2, s3, s4, s5
    };
    for (int i = 0; i < 5; i++) {
        std::cout << i + 1 << "." << "\n";
        solutions[i]();
        std::cout << std::endl;
    }
    return 0;
}