#ifndef TRAPEZOIDAL_METHOD_H
#define TRAPEZOIDAL_METHOD_H

#include "Lab5/IntegrationMethodsCommon.h"

std::vector<Matrix> trapezoidal(const Matrix& a, const Matrix& b, const RFunction& r,
                                const Matrix& x0, double T, double t_max);

std::vector<Matrix> trapezoidal(const Matrix& a, const Matrix& x0, double T, double t_max);

#endif //TRAPEZOIDAL_METHOD_H
