#include <iostream>
#include <random>
#include "Lab2/ObjectiveFunctions.h"
#include "Lab2/OptimizationAlgorithms.h"
#include "Lab2/Util.h"

void printTable(std::vector<std::vector<std::string>>& table, int min_cell = 10) {
    if (table.empty()) return;

    std::vector<int> lengths(table[0].size(), min_cell);
    for (auto& row : table) {
        for (int j = 0; j < (int) row.size(); j++) {
            int cell_size = row[j].size();
            if (cell_size > lengths[j]) {
                lengths[j] = cell_size;
            }
        }
    }

    table.insert(table.begin() + 1, std::vector<std::string>());
    for (int cell_length : lengths) {
        std::stringstream ss;
        for (int i = 0; i < cell_length; i++) {
            ss << "=";
        }
        table[1].push_back(ss.str());
    }

    for (auto& row : table) {
        for (int i = 0; i < (int) row.size(); i++) {
            auto& cell = row[i];
            std::cout << cell;
            for (int j = 0; j < (int) (lengths[i] - cell.size()); j++) {
                std::cout << " ";
            }
            std::cout << "|";
        }
        std::cout << "\n";
    }
}

void s1() {
    std::vector<std::vector<std::string>> table;

    opt::SimpleFunction f_simple = [](double x) { return std::pow(x - 3.0, 2); };
    opt::Function f = [](const std::vector<double>& x) { return std::pow(x[0] - 3.0, 2); };
    std::cout << "\nf(x) = (x - 3)^2\n";

    opt::TrackedFunction<double> tracker_simple(f_simple);
    opt::TrackedFunction<std::vector<double>&> tracker(f);

    double x0_values[] = {10.0, 50.0, 100.0, 500.0, 1000.0};
    for (auto x0_simple : x0_values) {
        table.clear();
        table.push_back({"Algorithm:", "Point:", "Value:", "Function calls:"});

        std::vector<double> x0 = {x0_simple};
        std::cout << "\nx0 = " << x0_simple << "\n\n";

        tracker_simple.Reset();
        double solution = opt::GoldenSectionSearch(tracker_simple, x0_simple, 0.1);
        table.push_back({"Golden section", std::to_string(solution), std::to_string(f_simple(solution)),
                         std::to_string(tracker_simple.getCallCount())});

        tracker.Reset();
        solution = opt::CoordinateSearch(tracker, x0)[0];
        table.push_back({"Coordinate search", std::to_string(solution), std::to_string(f_simple(solution)),
                         std::to_string(tracker.getCallCount())});

        tracker.Reset();
        solution = opt::NelderMeadSimplexMethod(tracker, x0)[0];
        table.push_back({"Nelder-Meade Simplex", std::to_string(solution), std::to_string(f_simple(solution)),
                         std::to_string(tracker.getCallCount())});

        tracker.Reset();
        solution = opt::HookeJeevesMethod(tracker, x0)[0];
        table.push_back({"Hooke-Jeeves", std::to_string(solution), std::to_string(f_simple(solution)),
                         std::to_string(tracker.getCallCount())});

        printTable(table);
    }
}

void s2() {
    std::vector<std::vector<std::string>> table;

    opt::Function functions[] = {
            opt::rosenbrock_banana_function,
            opt::second_function,
            opt::third_function,
            opt::jakobovic_function
    };

    const char* function_names[] = {
            "Rosenbrock's function",
            "Second function",
            "Third function",
            "Jakobovic's function"
    };

    std::vector<double> starting_points[] = {
            {-1.9, 2.0},
            {0.1,  0.3},
            {0.0,  0.0, 0.0, 0.0, 0.0},
            {5.1,  1.1}
    };

    for (int i = 0; i < 4; i++) {
        table.clear();
        table.push_back({"Algorithm:", "Point:", "Value:", "Function calls:"});

        opt::Function f = functions[i];
        std::vector<double> x0 = starting_points[i];
        std::cout << "\n" << function_names[i] << ", x0 = " << util::vectorToString(x0) << "\n\n";
        opt::TrackedFunction<std::vector<double>&> tracker(f);

        std::vector<double> solution = opt::NelderMeadSimplexMethod(tracker, x0);
        table.push_back({"Nelder-Mead Simplex", util::vectorToString(solution), std::to_string(f(solution)),
                         std::to_string(tracker.getCallCount())});

        tracker.Reset();
        solution = opt::HookeJeevesMethod(tracker, x0);
        table.push_back({"Hooke-Jeeves", util::vectorToString(solution), std::to_string(f(solution)),
                         std::to_string(tracker.getCallCount())});
        tracker.Reset();
        solution = opt::CoordinateSearch(tracker, x0);
        table.push_back({"Coordinate search", util::vectorToString(solution), std::to_string(f(solution)),
                         std::to_string(tracker.getCallCount())});

        printTable(table);
    }
}

void s3() {
    std::vector<std::vector<std::string>> table;

    opt::Function f = opt::jakobovic_function;
    opt::TrackedFunction<std::vector<double>&> tracker(f);
    std::vector<double> x0 = {5.0, 5.0};

    std::cout << "\nx0 = " << util::vectorToString(x0) << "\n\n";
    table.push_back({"Algorithm:", "Point:", "Value:", "Function calls:"});

    std::vector<double> solution = opt::HookeJeevesMethod(tracker, x0);
    table.push_back({"Hooke-Jeeves", util::vectorToString(solution), std::to_string(f(solution)),
                     std::to_string(tracker.getCallCount())});

    tracker.Reset();
    solution = opt::NelderMeadSimplexMethod(tracker, x0, true);
    table.push_back({"Nelder-Mead Simplex", util::vectorToString(solution), std::to_string(f(solution)),
                     std::to_string(tracker.getCallCount())});

    std::cout << "\n";
    printTable(table);
}

void s4() {
    opt::Function f = opt::rosenbrock_banana_function;
    opt::TrackedFunction<std::vector<double>&> tracker(f);
    std::vector<double> starting_points[] = {
            {0.5,  0.5},
            {20.0, 20.0}
    };

    for (auto x0 : starting_points) {
        std::vector<std::vector<std::string>> table;
        table.push_back({"dx_0:", "Point:", "Value:", "Function calls:"});
        std::cout << "\nx0 = " << util::vectorToString(x0) << "\n\n";

        for (int i = 1; i <= 20; i++) {
            std::vector<double> solution = opt::NelderMeadSimplexMethod(tracker, x0, false, i);
            table.push_back({std::to_string(i), util::vectorToString(solution), std::to_string(f(solution)),
                             std::to_string(tracker.getCallCount())});
            tracker.Reset();
        }

        printTable(table);
    }
}

void s5() {
    std::vector<std::vector<std::string>> table;
    table.push_back({"Point:", "Value:", "Function calls:"});
    std::cout << "\n";

    opt::Function f = opt::schaffer_function;
    opt::TrackedFunction<std::vector<double>&> tracker(f);

    std::random_device dev;
    std::default_random_engine re(dev());
    std::uniform_real_distribution<double> dist(-50.0, 50.0);

    int global_min_count = 0;
    const int run_count = 20000;
    for (int i = 0; i < run_count; i++) {
        std::vector<double> x0 = {dist(re), dist(re)};
        std::vector<double> solution = opt::NelderMeadSimplexMethod(tracker, x0);

        double f_solution = f(solution);
        if (f_solution < 1e-4) {
            global_min_count++;
            table.push_back({util::vectorToString(solution), std::to_string(f_solution),
                             std::to_string(tracker.getCallCount())});
        }

        tracker.Reset();
    }

    printTable(table);
    std::cout << "\n" << global_min_count << "/" << run_count << " global minimums found.\n"
              << "P = " << global_min_count * 100.0 / run_count << "%";
}

int main() {
    void (* solutions[])() = {
            s1, s2, s3, s4, s5
    };
    for (int i = 0; i < 5; i++) {
        std::cout << i + 1 << "." << "\n";
        solutions[i]();
        std::cout << std::endl;
    }
    return 0;
}