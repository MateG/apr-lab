#include "Lab5/IntegrationUtil.h"
#include "Lab5/EulerMethod.h"
#include "Lab5/BackwardEulerMethod.h"
#include "Lab5/TrapezoidalMethod.h"
#include "Lab5/RungeKuttaMethod.h"
#include "Lab5/PredictorCorrectorMethod.h"

int main() {
    const double T = 0.01;
    const double t_max = 1.0;

    Matrix a("resources/2_a.txt");
    Matrix x0("resources/2_x0.txt");

    std::cout << "EULER:\n";
    printSteps(euler(a, x0, T, t_max), T);

    std::cout << "BACKWARD EULER:\n";
    printSteps(backwardEuler(a, x0, T, t_max), T);

    std::cout << "TRAPEZOIDAL:\n";
    printSteps(trapezoidal(a, x0, T, t_max), T);

    std::cout << "RUNGE-KUTTA:\n";
    printSteps(fourthOrderRungeKutta(a, x0, T, t_max), T);

    std::cout << "PE(CE)^2:\n";
    printSteps(eulerBackwardEulerPECE2(a, x0, T, t_max), T);

    std::cout << "PECE:\n";
    printSteps(eulerTrapezoidalPECE(a, x0, T, t_max), T);

    return 0;
}
