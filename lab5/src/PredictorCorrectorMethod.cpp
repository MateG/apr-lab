#include "Lab5/PredictorCorrectorMethod.h"

typedef std::function<Matrix(const Matrix& x, double t,
                             const Matrix& a, const Matrix& b,
                             const std::function<Matrix(double)>& r, const double T)> Predictor;

Predictor predictor_euler = []( // NOLINT(cert-err58-cpp)
        const Matrix& x, double t,
        const Matrix& a, const Matrix& b, const RFunction& r, const double T) {
    return x + x_derivation(x, t, a, b, r) * T;
};

typedef std::function<Matrix(const Matrix& x, double t, const Matrix& predicted,
                             const Matrix& a, const Matrix& b,
                             const std::function<Matrix(double)>& r, const double T)> Corrector;

Corrector corrector_backward_euler = []( // NOLINT(cert-err58-cpp)
        const Matrix& x, double t, const Matrix& predicted,
        const Matrix& a, const Matrix& b, const RFunction& r, const double T) {
    return x + x_derivation(predicted, t + T, a, b, r) * T;
};

Corrector corrector_trapezoidal = []( // NOLINT(cert-err58-cpp)
        const Matrix& x, double t, const Matrix& predicted,
        const Matrix& a, const Matrix& b, const RFunction& r, const double T) {
    return x + (x_derivation(x, t, a, b, r) + x_derivation(predicted, t + T, a, b, r)) * (T / 2.0);
};

std::vector<Matrix> predictorCorrector(const Predictor& predictor, const Corrector& corrector,
                                       const int corrector_count,
                                       const Matrix& a,
                                       const Matrix& b, const std::function<Matrix(double)>& r,
                                       const Matrix& x0, const double T, const double t_max) {
    std::function<Matrix(const Matrix&, double)> calculate_next = [&](const Matrix& x, double t) {
        Matrix prediction = predictor(x, t, a, b, r, T);
        for (int i = 0; i < corrector_count; i++) {
            prediction = corrector(x, t, prediction, a, b, r, T);
        }
        return prediction;
    };
    return integrationMethod(calculate_next, x0, T, t_max);
}

std::vector<Matrix> predictorCorrector(const Predictor& predictor, const Corrector& corrector,
                                       const int corrector_count,
                                       const Matrix& a,
                                       const Matrix& x0, const double T, const double t_max) {
    Matrix b(a.getRows(), a.getCols());
    Matrix r_zero(x0.getRows(), x0.getCols());
    std::function<Matrix(double)> r = [r_zero](double t) {
        return r_zero;
    };
    return predictorCorrector(predictor, corrector, corrector_count, a, b, r, x0, T, t_max);
}

std::vector<Matrix> eulerBackwardEulerPECE2(const Matrix& a, const Matrix& x0,
                                            const double T, const double t_max) {
    return predictorCorrector(predictor_euler, corrector_backward_euler, 2, a, x0, T, t_max);
}

std::vector<Matrix> eulerBackwardEulerPECE2(const Matrix& a,
                                            const Matrix& b, const RFunction& r,
                                            const Matrix& x0,
                                            const double T, const double t_max) {
    return predictorCorrector(predictor_euler, corrector_backward_euler, 2, a, b, r, x0, T, t_max);
}

std::vector<Matrix> eulerTrapezoidalPECE(const Matrix& a, const Matrix& x0,
                                         const double T, const double t_max) {
    return predictorCorrector(predictor_euler, corrector_trapezoidal, 1, a, x0, T, t_max);
}

std::vector<Matrix> eulerTrapezoidalPECE(const Matrix& a,
                                         const Matrix& b, const RFunction& r,
                                         const Matrix& x0,
                                         const double T, const double t_max) {
    return predictorCorrector(predictor_euler, corrector_trapezoidal, 1, a, b, r, x0, T, t_max);
}
