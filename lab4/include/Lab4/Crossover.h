#ifndef CROSSOVER_H
#define CROSSOVER_H

#include <vector>
#include "BitUtil.h"

namespace opt {

    template<class C>
    class Crossover {
    public:
        C Cross(C& first, C& second) = 0;
    };

    template<>
    class Crossover<std::vector<double>> {

    private:
        std::random_device rd_;
        std::mt19937 mt_engine_ = std::mt19937(rd_());
        std::uniform_real_distribution<> uniform_ = std::uniform_real_distribution<>(0.0, 1.0);

    public:
        std::vector<double> Cross(std::vector<double>& first,
                                  std::vector<double>& second) {
            const double blxAlphaProbability = 0.75;

            if (uniform_(mt_engine_) < blxAlphaProbability) {
                return blxAlphaCross(first, second);
            } else {
                return arithmeticMeanCross(first, second);
            }
        }

    private:
        std::vector<double> blxAlphaCross(std::vector<double>& first,
                                          std::vector<double>& second) {
            const double alpha = 0.99;

            int size = first.size();
            std::vector<double> crossed(size);

            for (int i = 0; i < size; i++) {
                double lower = first[i];
                double upper = second[i];

                if (lower > upper) {
                    double lowerCopy = lower;
                    lower = upper;
                    upper = lowerCopy;
                }

                double extension = (upper - lower) * alpha;
                lower -= extension;
                upper += extension;
                std::uniform_real_distribution<> range = std::uniform_real_distribution<>(lower, upper);
                crossed[i] = range(mt_engine_);
            }

            return crossed;
        }

        std::vector<double> arithmeticMeanCross(std::vector<double>& first,
                                                std::vector<double>& second) {
            int size = first.size();
            std::vector<double> crossed(size);

            for (int i = 0; i < size; i++) {
                crossed[i] = (first[i] + second[i]) / 2.0;
            }

            return crossed;
        }
    };

    template<>
    class Crossover<std::vector<unsigned long>> {

    private:

        int bit_count_;

        std::random_device rd_;
        std::mt19937 mt_engine_ = std::mt19937(rd_());
        std::uniform_real_distribution<> uniform_ = std::uniform_real_distribution<>(0.0, 1.0);

    public:

        explicit Crossover(int bit_count) : bit_count_(bit_count) {}

        Crossover(Crossover<std::vector<unsigned long>>&& other) noexcept
                : bit_count_(other.bit_count_) {}

        std::vector<unsigned long> Cross(std::vector<unsigned long>& first,
                                         std::vector<unsigned long>& second) {
            const double uniformProbability = 0.75;

            if (uniform_(mt_engine_) < uniformProbability) {
                return uniformCross(first, second);
            } else {
                return singlePointCross(first, second);
            }
        }

    private:
        std::vector<unsigned long> uniformCross(std::vector<unsigned long>& first,
                                                std::vector<unsigned long>& second) {
            int size = first.size();
            std::vector<unsigned long> crossed(size);

            for (int i = 0; i < size; i++) {
                auto first_bits = first[i];
                auto second_bits = second[i];
                auto& crossed_bits = crossed[i];

                for (int j = 0; j < bit_count_; j++) {
                    unsigned int bit_value = bits::getBit(uniform_(mt_engine_) < 0.5 ?
                                                          first_bits : second_bits, j);
                    bits::setBit(crossed_bits, j, bit_value);
                }
            }

            return crossed;
        }

        std::vector<unsigned long> singlePointCross(std::vector<unsigned long>& first,
                                                    std::vector<unsigned long>& second) {
            int size = first.size();
            std::vector<unsigned long> crossed(size);

            std::uniform_int_distribution<> index_uniform(0, bit_count_ - 1);
            for (int i = 0; i < size; i++) {
                auto first_bits = first[i];
                auto second_bits = second[i];
                auto& crossed_bits = crossed[i];

                int random_index = index_uniform(mt_engine_);
                for (int j = 0; j < random_index; j++) {
                    bits::invertBit(crossed_bits, j);
                }
            }

            return crossed;
        }
    };
}

#endif //CROSSOVER_H
