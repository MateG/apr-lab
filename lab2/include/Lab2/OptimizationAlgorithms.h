#ifndef OPTIMIZATION_ALGORITHMS_H
#define OPTIMIZATION_ALGORITHMS_H

#define GOLDEN_RATIO 0.6180339887
#define DEFAULT_EPSILON 1e-6
#define DEFAULT_DX 0.5
#define MAX_ITERATIONS 100000

#include "ObjectiveFunctionTypes.h"
#include "Util.h"

namespace opt {

    /** Boundaries of an interval */
    struct Interval {
        double a, b;
    };

    /**
     * Finds he specified function's unimodal interval.
     *
     * @tparam Callable Specified function type.
     * @param f Specified function.
     * @param x0 Starting point.
     * @param h Left/right initial step.
     * @return Found unimodal interval.
     */
    template<class Callable>
    Interval UnimodalInterval(Callable& f, double x0, double h) {
        unsigned int step = 1;

        double m = x0;
        double l = x0 - h;
        double r = x0 + h;

        double fm = f(m);
        double fl = f(l);
        double fr = f(r);

        if (!(fm < fr && fm < fl)) {
            if (fm > fr) {
                do {
                    l = m;
                    m = r;
                    fm = fr;
                    step *= 2;
                    r = x0 + h * step;
                    fr = f(r);
                } while (fm > fr);
            } else {
                do {
                    r = m;
                    m = l;
                    fm = fl;
                    step *= 2;
                    l = x0 - h * step;
                    fl = f(l);
                } while (fm > fl);
            }
        }

        return {l, r};
    }

    /**
     * Performs the golden section algorithm on the specified function.
     *
     * @tparam Callable Specified function type.
     * @param f Specified function.
     * @return Found point of minimum.
     */
    template<class Callable>
    double GoldenSectionSearch(Callable& f) {
        return GoldenSectionSearch(f, opt::UnimodalInterval(f, 0.0, 1.0));
    }

    /**
     * Performs the golden section algorithm on the specified function.
     *
     * @tparam Callable Specified function type.
     * @param f Specified function.
     * @param x0 Starting point (for unimodal interval search).
     * @param h Left/right initial step (for unimodal interval search).
     * @return Found point of minimum.
     */
    template<class Callable>
    double GoldenSectionSearch(Callable& f, double x0, double h) {
        return GoldenSectionSearch(f, opt::UnimodalInterval(f, x0, h));
    }

    /**
     * Performs the golden section algorithm on the specified function.
     *
     * @tparam Callable Specified function type.
     * @param f Specified function.
     * @param interval Specified interval.
     * @param epsilon Breaking condition value.
     * @return Found point of minimum.
     */
    template<class Callable>
    double GoldenSectionSearch(Callable& f, opt::Interval interval, double epsilon = DEFAULT_EPSILON) {
        double c = interval.b - GOLDEN_RATIO * (interval.b - interval.a);
        double d = interval.a + GOLDEN_RATIO * (interval.b - interval.a);
        double fc = f(c);
        double fd = f(d);

        while (true) {
            if (fc < fd) {
                interval.b = d;
                if (interval.b - interval.a <= epsilon) break;

                d = c;
                c = interval.b - GOLDEN_RATIO * (interval.b - interval.a);
                fd = fc;
                fc = f(c);
            } else {
                interval.a = c;
                if (interval.b - interval.a <= epsilon) break;

                c = d;
                d = interval.a + GOLDEN_RATIO * (interval.b - interval.a);
                fc = fd;
                fd = f(d);
            }
        }

        return (interval.a + interval.b) / 2.0;
    }

    /**
     * Performs the coordinate search algorithm on the specified function.
     *
     * @tparam Callable Specified function type.
     * @param f Specified function.
     * @param x0 Starting point.
     * @return Found point of minimum.
     */
    template<class Callable>
    VectorArg CoordinateSearch(Callable& f, VectorArg& x0) {
        std::vector<double> epsilon(x0.size());
        for (int i = 0; i < (int) x0.size(); i++) epsilon[i] = DEFAULT_EPSILON;
        return CoordinateSearch(f, x0, epsilon);
    }

    /**
     * Performs the coordinate search algorithm on the specified function.
     *
     * @tparam Callable Specified function type.
     * @param f Specified function.
     * @param x0 Starting point.
     * @param epsilon Breaking condition vector.
     * @return Found point of minimum.
     */
    template<class Callable>
    VectorArg CoordinateSearch(Callable& f, VectorArg& x0, std::vector<double> epsilon) {
        VectorArg x = x0;
        int n = x.size();

        while (true) {
            VectorArg xs = x;
            for (int i = 0; i < n; i++) {
                SimpleFunction simple_f = [&](double lambda) mutable {
                    VectorArg new_x = x;
                    new_x[i] += lambda;
                    return f(new_x);
                };

                double lambda = GoldenSectionSearch(simple_f);
                x[i] += lambda;
            }

            bool end_reached = true;
            for (int i = 0; i < n; i++) {
                double delta = x[i] - xs[i];
                if (std::abs(delta) > epsilon[i]) {
                    end_reached = false;
                    break;
                }
            }
            if (end_reached) break;
        }

        return x;
    }

    /**
     * Performs the Nelder-Mead simplex algorithm on the specified function.
     *
     * @tparam Callable Specified function type.
     * @param f Specified function.
     * @param x0 Starting point.
     * @param dx0 Initial delta x used for simplex generation.
     * @param alpha Specified reflection parameter.
     * @param beta Specified contraction parameter.
     * @param gamma Specified expansion parameter.
     * @param sigma Specified movement parameter.
     * @param epsilon Breaking condition value.
     * @return Found point of minimum.
     */
    template<class Callable>
    VectorArg NelderMeadSimplexMethod(Callable& f, VectorArg& x0, bool log = false, double dx0 = 1.0,
                                      double alpha = 1.0, double beta = 0.5, double gamma = 2.0,
                                      double sigma = 0.5, double epsilon = DEFAULT_EPSILON) {
        int size = x0.size();

        // Initialize helper lambdas
        auto end_reached = [size, f, epsilon](std::vector<VectorArg>& simplex, double fc) mutable {
            double value = 0.0;
            for (int i = 0; i < size + 1; i++) {
                double f_i = f(simplex[i]);
                value += std::pow(f_i - fc, 2);
            }
            value /= size;
            return std::sqrt(value) <= epsilon;
        };
        auto max_index = [f](std::vector<VectorArg>& simplex) mutable {
            int h = 0;
            double max = f(simplex[0]);
            for (int i = 1; i < (int) simplex.size(); i++) {
                double f_i = f(simplex[i]);
                if (f_i > max) {
                    h = i;
                    max = f_i;
                }
            }
            return h;
        };
        auto min_index = [f](std::vector<VectorArg>& simplex) mutable {
            int l = 0;
            double min = f(simplex[0]);
            for (int i = 1; i < (int) simplex.size(); i++) {
                double f_i = f(simplex[i]);
                if (f_i < min) {
                    l = i;
                    min = f_i;
                }
            }
            return l;
        };
        auto reflection = [size, alpha](VectorArg& xc, VectorArg& xh) {
            VectorArg r(size);
            for (int i = 0; i < size; i++) r[i] = (1 + alpha) * xc[i] - alpha * xh[i];
            return r;
        };
        auto expansion = [size, gamma](VectorArg& xc, VectorArg& xr) {
            VectorArg e(size);
            for (int i = 0; i < size; i++) e[i] = (1 - gamma) * xc[i] + gamma * xr[i];
            return e;
        };
        auto contraction = [size, beta](VectorArg& xc, VectorArg& xh) {
            VectorArg c(size);
            for (int i = 0; i < size; i++) c[i] = (1 - beta) * xc[i] + beta * xh[i];
            return c;
        };

        // Create initial simplex
        std::vector<VectorArg> simplex(size + 1);
        simplex[0] = x0;
        for (int i = 1; i <= size; i++) {
            VectorArg x_i(x0);
            x_i[i - 1] += dx0;
            simplex[i] = x_i;
        }

        VectorArg xc;
        for (int iter = 0; iter < MAX_ITERATIONS; iter++) {
            // Find min/max indexes
            int h = max_index(simplex);
            int l = min_index(simplex);
            double fl = f(simplex[l]);

            // Find the centroid
            xc = VectorArg(size, 0.0);
            for (int i = 0; i < size + 1; i++) {
                if (i == h) continue;

                for (int j = 0; j < size; j++) {
                    xc[j] += simplex[i][j];
                }
            }
            for (int i = 0; i < size; i++) {
                xc[i] /= size;
            }
            double fc = f(xc);

            // Print current centroid
            if (log) {
                std::cout << "[" << iter << "]"
                          << " Xc = " << util::vectorToString(xc)
                          << " f(Xc) = " << fc << "\n";
            }

            // Check if end reached
            if (end_reached(simplex, fc)) break;

            // Find reflection
            VectorArg xr = reflection(xc, simplex[h]);
            double fr = f(xr);

            if (fr < fl) {
                VectorArg xe = expansion(xc, xr);
                double fe = f(xe);

                if (fe < fl) {
                    simplex[h] = xe;
                } else {
                    simplex[h] = xr;
                }
            } else {
                bool fr_greatest = true;
                for (int j = 0; j < size; j++) {
                    if (j == h) continue;

                    double fj = f(simplex[j]);
                    if (fr < fj) {
                        fr_greatest = false;
                        break;
                    }
                }

                if (fr_greatest) {
                    double fh = f(simplex[h]);
                    if (fr < fh) simplex[h] = xr;

                    VectorArg xk = contraction(xc, simplex[h]);
                    double fk = f(xk);

                    if (fk < f(simplex[h])) {
                        simplex[h] = xk;
                    } else {
                        // Move all points towards simplex[l]
                        VectorArg xl = simplex[l];
                        for (auto& point : simplex) {
                            for (int i = 0; i < (int) point.size(); i++) {
                                point[i] += sigma * (xl[i] - point[i]);
                            }
                        }
                    }
                } else {
                    simplex[h] = xr;
                }
            }
        }

        return xc;
    }

    /**
     * Performs the Hooke-Jeeves algorithm on the specified function.
     *
     * @tparam Callable Specified function type.
     * @param f Specified function.
     * @param x0 Starting point.
     * @return Found point of minimum.
     */
    template<class Callable>
    VectorArg HookeJeevesMethod(Callable& f, VectorArg& x0) {
        int n = x0.size();

        VectorArg dx(n);
        for (int i = 0; i < n; i++) dx[i] = DEFAULT_DX;

        VectorArg epsilon(n);
        for (int i = 0; i < n; i++) epsilon[i] = DEFAULT_EPSILON;

        return HookeJeevesMethod(f, x0, dx, epsilon);
    }

    /**
     * Performs the Hooke-Jeeves algorithm on the specified function.
     *
     * @tparam Callable Specified function type.
     * @param f Specified function.
     * @param x0 Starting point.
     * @param dx Movement vector.
     * @param epsilon Breaking condition vector.
     * @return Found point of minimum.
     */
    template<class Callable>
    VectorArg HookeJeevesMethod(Callable& f, VectorArg& x0, VectorArg& dx, const VectorArg& epsilon) {
        VectorArg xp = x0;
        VectorArg xb = x0;
        int size = x0.size();

        while (true) {
            VectorArg xn = xp;
            double n = 0.0;
            for (int i = 0; i < size; i++) {
                double p = f(xn);
                xn[i] += dx[i];
                n = f(xn);

                if (n > p) {
                    xn[i] -= 2 * dx[i];
                    n = f(xn);
                    if (n > p) {
                        xn[i] += dx[i];
                        if (i == size - 1) n = f(xn);
                    }
                }
            }

            if (n < f(xb)) {
                for (int i = 0; i < size; i++) {
                    xp[i] = 2 * xn[i] - xb[i];
                }
                xb = xn;
            } else {
                for (double& i : dx) i /= 2.0;
                xp = xb;
            }

            bool end_reached = true;
            for (int i = 0; i < size; i++) {
                if (dx[i] > epsilon[i]) {
                    end_reached = false;
                    break;
                }
            }
            if (end_reached) break;
        }

        return xb;
    }
}


#endif //OPTIMIZATION_ALGORITHMS_H
