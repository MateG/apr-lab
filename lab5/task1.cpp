#include <iostream>
#include <cmath>
#include "Lab5/IntegrationUtil.h"
#include "Lab5/EulerMethod.h"
#include "Lab5/BackwardEulerMethod.h"
#include "Lab5/TrapezoidalMethod.h"
#include "Lab5/RungeKuttaMethod.h"
#include "Lab5/PredictorCorrectorMethod.h"

double calculateError(const std::vector<Matrix>& x_all, const Matrix& x0, const double T) {
    const double x1_0 = x0.Get(0);
    const double x2_0 = x0.Get(1);
    const auto x1 = [x1_0, x2_0](double t) {
        return x1_0 * std::cos(t) + x2_0 * std::sin(t);
    };
    const auto x2 = [x1_0, x2_0](double t) {
        return x2_0 * std::cos(t) - x1_0 * std::sin(t);
    };

    double error = 0.0;
    double t = 0.0;
    for (const Matrix& x : x_all) {
        Matrix expected(2, 1, {x1(t), x2(t)});
        error += (expected - x).Norm();
        t += T;
    }
    return error;
}

int main() {
    const double T = 0.01;
    const double t_max = 10.0;
    const int log_period = 10;

    Matrix a("resources/1_a.txt");
    Matrix x0("resources/1_x0.txt");

    std::cout << "EULER:\n";
    printSteps(euler(a, x0, T, t_max), T, log_period);

    std::cout << "BACKWARD EULER:\n";
    printSteps(backwardEuler(a, x0, T, t_max), T, log_period);

    std::cout << "TRAPEZOIDAL:\n";
    printSteps(trapezoidal(a, x0, T, t_max), T, log_period);

    std::cout << "RUNGE-KUTTA:\n";
    printSteps(fourthOrderRungeKutta(a, x0, T, t_max), T, log_period);

    std::cout << "PE(CE)^2:\n";
    printSteps(eulerBackwardEulerPECE2(a, x0, T, t_max), T, log_period);

    std::cout << "PECE:\n";
    printSteps(eulerTrapezoidalPECE(a, x0, T, t_max), T, log_period);

    std::cout << "ERRORS:\n";
    std::cout << calculateError(euler(a, x0, T, t_max), x0, T) << "\n"
              << calculateError(backwardEuler(a, x0, T, t_max), x0, T) << "\n"
              << calculateError(trapezoidal(a, x0, T, t_max), x0, T) << "\n"
              << calculateError(fourthOrderRungeKutta(a, x0, T, t_max), x0, T) << "\n"
              << calculateError(eulerBackwardEulerPECE2(a, x0, T, t_max), x0, T) << "\n"
              << calculateError(eulerTrapezoidalPECE(a, x0, T, t_max), x0, T) << "\n";

    return 0;
}