#include <iostream>
#include <iomanip>
#include "Lab1/Matrix.h"

void handleInvalidArgument(const std::invalid_argument& e) {
    std::cout << "FAILED: " << e.what() << "\n";
}

void trySolvingWithLu(Matrix& a, Matrix& b) {
    try {
        std::cout << "\nTrying LU decomposition...\n";
        Matrix lu = a.LuDecomposition();
        std::cout << "\nLU:\n" << lu;
        Matrix x = lu.BackSubstitution(lu.ForwardSubstitution(b));
        std::cout << "\nx:\n" << x;
    } catch (std::invalid_argument& e) {
        handleInvalidArgument(e);
    }
}

void trySolvingWithLup(Matrix& a, Matrix& b) {
    try {
        std::cout << "\nTrying LUP decomposition...\n";
        std::vector<int> P;
        Matrix lu = a.LupDecomposition(P);
        std::cout << "\nLU:\n" << lu;
        Matrix b_permuted = b.Permutation(P);
        std::cout << "\nb (permuted):\n" << b_permuted;
        Matrix x = lu.BackSubstitution(lu.ForwardSubstitution(b_permuted));
        std::cout << "\nx:\n" << x;
    } catch (std::invalid_argument& e) {
        handleInvalidArgument(e);
    }
}

void trySolvingWithLuAndLup(Matrix& a, Matrix& b) {
    trySolvingWithLu(a, b);
    trySolvingWithLup(a, b);
}

void tryCalculatingInverse(Matrix& a) {
    try {
        std::cout << "\nTrying to calculate matrix inverse...\n";
        Matrix a_i = !a;
        std::cout << "\nA^-1:\n" << a_i << "\n";
    } catch (std::invalid_argument& e) {
        handleInvalidArgument(e);
    }
}

void tryCalculatingDeterminant(Matrix& a) {
    try {
        std::cout << "\nTrying to calculate determinant...\n";
        double determinant = a.Determinant();
        std::cout << "\ndet(A) = " << determinant << "\n";
    } catch (std::invalid_argument& e) {
        handleInvalidArgument(e);
    }
}

void s1() {
    Matrix a("data/1.txt");
    Matrix b = a;

    double factor = 99999.99999999;
    b *= factor;
    b *= 1.0 / factor;

    std::cout << "\nA (original):\n" << std::setprecision(20) << a
              << "\nB (multiplied then divided):\n" << b
              << "\nA == B ? " << (a == b ? "TRUE" : "FALSE") << "\n";
}

void s2() {
    Matrix a("data/2a.txt");
    Matrix b("data/2b.txt");
    std::cout << "\nA:\n" << a << "\nb:\n" << b;
    trySolvingWithLuAndLup(a, b);
}

void s3() {
    Matrix a("data/3a.txt");
    Matrix b("data/3b.txt");
    std::cout << "\nA:\n" << a << "\nb:\n" << b;
    trySolvingWithLuAndLup(a, b);
}

void s4() {
    Matrix a("data/4a.txt");
    Matrix b("data/4b.txt");
    std::cout << "\nA:\n" << a << "\nb:\n" << b;
    trySolvingWithLuAndLup(a, b);
}

void s5() {
    Matrix a("data/5a.txt");
    Matrix b("data/5b.txt");
    std::cout << "\nA:\n" << a << "\nb:\n" << b;
    trySolvingWithLup(a, b);
}

void s6() {
    Matrix a("data/6a.txt");
    Matrix b("data/6b.txt");
    std::cout << "\nA:\n" << a << "\nb:\n" << b;
    trySolvingWithLuAndLup(a, b);
}

void s7() {
    Matrix a("data/7.txt");
    std::cout << "\nA:\n" << a;
    tryCalculatingInverse(a);
}

void s8() {
    Matrix a("data/8.txt");
    std::cout << "\nA:\n" << a;
    tryCalculatingInverse(a);
}

void s9() {
    Matrix a("data/9.txt");
    std::cout << "\nA:\n" << a;
    tryCalculatingDeterminant(a);
}

void s10() {
    Matrix a("data/10.txt");
    std::cout << "\nA:\n" << a;
    tryCalculatingDeterminant(a);
}

int main() {
    void (* solutions[10])() = {
            s1, s2, s3, s4, s5, s6, s7, s8, s9, s10
    };
    for (int i = 0; i < 10; i++) {
        std::cout << i + 1 << "." << "\n";
        solutions[i]();
        std::cout << "\n";
    }
    return 0;
}