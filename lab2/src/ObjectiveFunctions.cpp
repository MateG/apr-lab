#include "Lab2/ObjectiveFunctions.h"

#include <cmath>

using namespace opt;

Function opt::rosenbrock_banana_function = [](VectorArg& x) { // NOLINT(cert-err58-cpp)
    double x1 = x[0];
    double x2 = x[1];

    return 100.0 * std::pow(x2 - x1 * x1, 2) + std::pow(1.0 - x1, 2);
};

Function opt::second_function = [](VectorArg& x) { // NOLINT(cert-err58-cpp)
    double x1 = x[0];
    double x2 = x[1];

    return std::pow(x1 - 4.0, 2) + 4.0 * std::pow(x2 - 2, 2);
};

Function opt::third_function = [](VectorArg& x) { // NOLINT(cert-err58-cpp)
    double sum = 0.0;
    for (int i = 0; i < (int) x.size(); i++) {
        double difference = x[i] - (i + 1);
        sum += difference * difference;
    }

    return sum;
};

Function opt::jakobovic_function = [](VectorArg& x) { // NOLINT(cert-err58-cpp)
    double x1_squared = x[0] * x[0];
    double x2_squared = x[1] * x[1];

    return std::abs(x1_squared - x2_squared) + std::sqrt(x1_squared + x2_squared);
};

Function opt::schaffer_function = [](VectorArg& x) { // NOLINT(cert-err58-cpp)
    double sum = 0.0;
    for (auto x_i : x) {
        sum += x_i * x_i;
    }

    double numerator = std::pow(std::sin(std::sqrt(sum)), 2) - 0.5;
    double denominator = std::pow(1 + 0.001 * sum, 2);

    return 0.5 + numerator / denominator;
};
