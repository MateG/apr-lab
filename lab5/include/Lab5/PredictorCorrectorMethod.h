#ifndef PREDICTOR_CORRECTOR_METHOD_H
#define PREDICTOR_CORRECTOR_METHOD_H

#include "Lab5/IntegrationMethodsCommon.h"

typedef std::function<Matrix(
        const Matrix& x, double t,
        const Matrix& a, const Matrix& b, const RFunction& r, const double T)> Predictor;

extern Predictor predictor_euler;

typedef std::function<Matrix(
        const Matrix& x, double t, const Matrix& predicted,
        const Matrix& a, const Matrix& b, const RFunction& r, const double T)> Corrector;

extern Corrector corrector_backward_euler;

extern Corrector corrector_trapezoidal;

std::vector<Matrix> predictorCorrector(const Predictor& predictor, const Corrector& corrector,
                                       int corrector_count,
                                       const Matrix& a,
                                       const Matrix& b, const RFunction& r,
                                       const Matrix& x0, double T, double t_max);

std::vector<Matrix> predictorCorrector(const Predictor& predictor, const Corrector& corrector,
                                       int corrector_count,
                                       const Matrix& a,
                                       const Matrix& x0, double T, double t_max);

std::vector<Matrix> eulerBackwardEulerPECE2(const Matrix& a, const Matrix& x0,
                                            double T, double t_max);

std::vector<Matrix> eulerBackwardEulerPECE2(const Matrix& a,
                                            const Matrix& b, const RFunction& r,
                                            const Matrix& x0,
                                            double T, double t_max);

std::vector<Matrix> eulerTrapezoidalPECE(const Matrix& a, const Matrix& x0,
                                         double T, double t_max);

std::vector<Matrix> eulerTrapezoidalPECE(const Matrix& a,
                                         const Matrix& b, const RFunction& r,
                                         const Matrix& x0,
                                         double T, double t_max);

#endif //PREDICTOR_CORRECTOR_METHOD_H
