#ifndef EULER_METHOD_H
#define EULER_METHOD_H

#include "Lab5/IntegrationMethodsCommon.h"

std::vector<Matrix> euler(const Matrix& a, const IntegrationBiFunction& x_derivation,
                          const Matrix& x0, double T, double t_max);

std::vector<Matrix> euler(const Matrix& a, const Matrix& b, const RFunction& r,
                          const Matrix& x0, double T, double t_max);

std::vector<Matrix> euler(const Matrix& a, const Matrix& x0, double T, double t_max);

#endif //EULER_METHOD_H
