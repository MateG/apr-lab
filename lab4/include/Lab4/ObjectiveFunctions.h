#ifndef OBJECTIVE_FUNCTIONS_H
#define OBJECTIVE_FUNCTIONS_H

#include <cmath>
#include <functional>
#include <vector>

namespace opt {

    typedef std::function<double(std::vector<double>&)> Function;

    /**
     * f(x) = 100 * (x2 - x1 ^ 2) ^ 2 + (1 - x1) ^ 2
     * x_min = (1, 1)
     * f_min = 0
     */
    extern Function f1;

    /**
     * f(x) = sum((xi - i) ^ 2)
     * x_min = (1, 2, 3, ..., n)
     * f_min = 0
     */
    extern Function f3;

    /**
     * f(x) = 0.5 + (sin^2(sqrt(xi ^ 2)) - 0.5) / (1 + 0.001 * sum(xi ^ 2)) ^ 2
     * x_min = (0, 0, 0, ..., 0)
     * f_min = 0
     */
    extern Function f6;

    /**
     * f(x) = (sum(xi ^ 2)) ^ 0.25 * (1 + sin^2(50 * (sum(xi ^ 2)) ^ 0.1))
     * x_min = (0, 0, 0, ..., 0)
     * f_min = 0
     */
    extern Function f7;

    /**
     * Wrapper class for std::function<double(std::vector<double>&)>
     * which counts the number of function calls.
     */
    class TrackedFunction {
    private:

        /** Wrapped function. */
        Function& function_;

        /** Function call counter. */
        int call_count_;

    public:

        /**
         * Constructor which specifies the wrapped function
         * and initializes the call count to 0.
         *
         * @param function Specified function.
         */
        explicit TrackedFunction(Function& function) : function_(function), call_count_(0) {};

        /**
         * Increments the call count and delegates
         * the call to the wrapped function.
         *
         * @param x Specified function argument.
         * @return Return value from the wrapped function call.
         */
        double operator()(std::vector<double>& x) {
            call_count_++;
            return function_(x);
        };

        /** @return Call count. */
        int getCallCount() const {
            return call_count_;
        }

        /** Resets the call count to 0. */
        void Reset() {
            call_count_ = 0;
        }
    };
}

#endif //OBJECTIVE_FUNCTIONS_H
