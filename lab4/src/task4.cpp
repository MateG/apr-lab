#include <iostream>
#include <iomanip>
#include "TasksUtil.h"

int main() {
    std::cout << std::setprecision(16);

    std::vector<double> mutation_probabilities = {0.1, 0.3, 0.6, 0.9};
    std::vector<int> population_sizes = {30, 50, 100, 200};

    std::vector<std::vector<std::vector<double>>> box_plot_data;

    for (auto mutation_probability : mutation_probabilities) {
        std::vector<std::vector<double>> mutation_box_plot_data;
        for (auto population_size : population_sizes) {
            std::vector<double> best_values = runMultipleDecimal(
                    opt::f6, 3, population_size, 3, mutation_probability, 20, 50000
            );
            std::cout << "Population size: " << population_size << "\n";
            std::cout << "Mutation probability: " << mutation_probability << "\n";
            printValues(best_values);
            printStatistics(best_values);
            std::cout << "\n";

            mutation_box_plot_data.push_back(best_values);
        }
        box_plot_data.push_back(mutation_box_plot_data);
    }

    for (int i = 0; i < int(mutation_probabilities.size()); i++) {
        std::cout << "\nBox plot data for mutation probability: "
                  << mutation_probabilities[i] << "\n";
        for (auto population_size : population_sizes) {
            std::cout << "POP" << population_size << ",";
        }
        std::cout << "\n";

        auto& mutation_box_plot_data = box_plot_data[i];
        for (int j = 0; j < int(box_plot_data[0].size()); j++) {
            for (int k = 0; k < int(box_plot_data.size()); k++) {
                std::cout << mutation_box_plot_data[j][k];
                if (k < box_plot_data.size() - 1) std::cout << ",";
            }
            std::cout << "\n";
        }
    }

    return 0;
}
