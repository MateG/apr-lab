#include "Lab5/IntegrationMethodsCommon.h"

std::vector<Matrix> integrationMethod(const IntegrationBiFunction& calculate_next,
                                      const Matrix& x0, const double T, const double t_max) {
    std::vector<Matrix> x_all;
    Matrix x_current = x0;
    x_all.push_back(x_current);

    for (double t = T; t <= t_max; t += T) { // NOLINT(cert-flp30-c)
        Matrix x_next = calculate_next(x_current, t);
        x_current = x_next;
        x_all.push_back(x_current);
    }

    return x_all;
}

Matrix identityMatrix(int rows) {
    Matrix u(rows, rows);
    for (int i = 0; i < rows; i++) {
        u[i][i] = 1.0;
    }
    return u;
}

Matrix x_derivation(const Matrix& x, double t,
                    const Matrix& a, const Matrix& b, const RFunction& r) {
    return a * x + b * r(t);
}
