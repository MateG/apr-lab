#ifndef OBJECTIVE_FUNCTION_TYPES_H
#define OBJECTIVE_FUNCTION_TYPES_H

#include <functional>
#include <vector>

namespace opt {

    /** Objective function's argument type. */
    typedef std::vector<double> VectorArg;

    /** Simple (of a real variable) objective function type. */
    typedef std::function<double(double)> SimpleFunction;

    /** Objective function type. */
    typedef std::function<double(VectorArg&)> Function;
}

#endif //OBJECTIVE_FUNCTION_TYPES_H
